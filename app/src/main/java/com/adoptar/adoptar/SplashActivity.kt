package com.adoptar.adoptar

import android.animation.Animator
import android.content.Intent
import android.media.MediaPlayer
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_splash.*
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.RandomAccess

class SplashActivity : AppCompatActivity() {

    // This is the loading time of the splash screen
    private val SPLASH_TIME_OUT:Long = 4000
    lateinit var bark: MediaPlayer
    lateinit var meow: MediaPlayer
    var mp3List: MutableList<MediaPlayer> = ArrayList<MediaPlayer>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        bark = MediaPlayer.create(this, R.raw.bark)
        meow = MediaPlayer.create(this, R.raw.meow)
        mp3List.add(bark)
        mp3List.add(meow)
        splashSplash.setAnimation(R.raw.splash)
        splashPaw.setAnimation(R.raw.pawscreen)
        splashPaw.playAnimation()

        splashPaw.addAnimatorListener(object : Animator.AnimatorListener {
             override fun onAnimationStart(animation: Animator) {
             }

             override fun onAnimationEnd(animation: Animator) {
                 mp3List.shuffled().take(1)[0].start()
                 splashSplash.playAnimation()
                 splashIco.visibility = View.VISIBLE

             }

             override fun onAnimationCancel(animation: Animator) {
             }

             override fun onAnimationRepeat(animation: Animator) {
             }
         })

        Handler().postDelayed({
            // This method will be executed once the timer is over

            startActivity(Intent(this, AuthActivity::class.java))

            // close this activity
            finish()
        }, SPLASH_TIME_OUT)
    }

}