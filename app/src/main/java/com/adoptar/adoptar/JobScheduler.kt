package com.adoptar.adoptar

import com.adoptar.adoptar.modules.notifications.domain.Notification
import com.adoptar.adoptar.modules.notifications.infraestructure.NotificationService
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

class JobScheduler(private var navView: BottomNavigationView, private var userId: String) :
    CoroutineScope {

    private var job: Job = Job()

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Default + job

        fun schedule() = launch {
            while (true) {
                var notif = NotificationService.getNotificationsById(userId)!!
                var read = false
                var i = 0
                while (notif.size > i && !read) {
                    if (!notif[i].isRead!!) {
                        navView.getOrCreateBadge(R.id.navigation_notifications).isVisible = true
                        read = true
                    } else {
                        i++
                    }
                }
                delay(60000)
            }
    }

    fun cancel() {
        job.cancel()
    }

}