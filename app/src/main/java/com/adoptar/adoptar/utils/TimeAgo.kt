package com.adoptar.adoptar.utils

import java.text.SimpleDateFormat

class TimeAgo {

    companion object{

        private val SECOND_MILLIS = 1000
        private val MINUTE_MILLIS = 60 * SECOND_MILLIS
        private val HOUR_MILLIS = 60 * MINUTE_MILLIS
        private val DAY_MILLIS = 24 * HOUR_MILLIS

        fun getTimeAgo(time: Long): String? {
            lateinit var timeSince: String
            var time = time
            if (time < 1000000000000L) {
                time *= 1000
            }
            val now = System.currentTimeMillis()
            if (time > now || time <= 0) {
                timeSince = "null"
            }
            val diff = now - time
            if (diff < MINUTE_MILLIS) {
                timeSince = "justo ahora"
            } else if (diff < 2 * MINUTE_MILLIS) {
                timeSince = "hace un minuto"
            } else if (diff < 50 * MINUTE_MILLIS) {
                timeSince = (diff / MINUTE_MILLIS).toString() + " minutos atras"
            } else if (diff < 90 * MINUTE_MILLIS) {
                timeSince = "an hour ago"
            } else if (diff < 24 * HOUR_MILLIS) {
                timeSince = (diff / HOUR_MILLIS).toString() + " horas atras"
            } else if (diff < 48 * HOUR_MILLIS) {
                timeSince = "ayer"
            } else {
                timeSince = (diff / DAY_MILLIS).toString() + " dias atras"
            }
            return timeSince
        }

        fun convertDateToLong(date: String): Long {
            val df = SimpleDateFormat("yyyy.MM.dd HH:mm")
            return df.parse(date).time
        }

    }
}