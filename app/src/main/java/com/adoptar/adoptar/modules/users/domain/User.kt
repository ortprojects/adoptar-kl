package com.adoptar.adoptar.modules.users.domain

import android.os.Build
import android.os.Parcel
import android.os.Parcelable
import androidx.annotation.RequiresApi

data class User(
    var address: Address?,
    var displayName: String?,
    var dni: String?,
    var email: String?,
    var id: String?,
    var phoneNumber: String?,
    val photoURL: String?,
    var isFirstTime: Boolean?,
): Parcelable{
    @RequiresApi(Build.VERSION_CODES.Q)
    constructor(parcel: Parcel?) : this(
        parcel?.readParcelable(Address::class.java.classLoader),
        parcel?.readString(),
        parcel?.readString(),
        parcel?.readString(),
        parcel?.readString(),
        parcel?.readString(),
        parcel?.readString(),
        parcel?.readBoolean()

    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeParcelable(address, flags)
        parcel.writeString(displayName)
        parcel.writeString(dni)
        parcel.writeString(email)
        parcel.writeString(id)
        parcel.writeString(phoneNumber)
        parcel.writeString(photoURL)
        parcel.writeString(isFirstTime.toString())

    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<User> {
        @RequiresApi(Build.VERSION_CODES.Q)
        override fun createFromParcel(parcel: Parcel): User {
            return User(parcel)
        }

        override fun newArray(size: Int): Array<User?> {
            return arrayOfNulls(size)
        }
    }

}