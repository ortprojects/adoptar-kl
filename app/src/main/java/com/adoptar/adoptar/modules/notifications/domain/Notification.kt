package com.adoptar.adoptar.modules.notifications.domain

import android.os.Parcel
import android.os.Parcelable

data class Notification(
    val createdAt: String?,
    val createdAtByUserId: String?,
    val id: String?,
    val isRead: Boolean?,
    val message: String?
): Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readBoolean(),
        parcel.readString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(createdAt)
        parcel.writeString(createdAtByUserId)
        parcel.writeString(id)
        parcel.writeString(message)
        parcel.writeBoolean(isRead!!)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Notification> {
        override fun createFromParcel(parcel: Parcel): Notification {
            return Notification(parcel)
        }

        override fun newArray(size: Int): Array<Notification?> {
            return arrayOfNulls(size)
        }
    }
}