package com.adoptar.adoptar.modules.posts.infrastructure

import android.util.Log
import com.adoptar.adoptar.modules.posts.domain.*
import com.adoptar.adoptar.modules.shared.http.HttpClient
import com.adoptar.adoptar.modules.users.domain.User
import com.adoptar.adoptar.ui.posts.fragments.PostDetailFragmentArgs
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.await
import retrofit2.awaitResponse
import java.lang.Exception

    class PostService {

    companion object{
        val httpClient = HttpClient.getHttpClient().create(PostRepository::class.java)

        suspend fun getPosts(latitude: String, longitude: String, radio: String, petMonitoring: String?, petCastratedType: String?, petAgeTypes: String?,
                             petSexType: String?, petSizeType: String?, petType: String?, postType: String?) : List<Post>? {
            var postsList : List<Post>? = listOf()
            try{
                postsList = httpClient.getPosts(latitude, longitude, radio, petMonitoring, petCastratedType, petAgeTypes, petSexType ,petSizeType, petType, postType).await()
            }catch(error : Exception){
                Log.d("error", error.message)
            }
            return postsList
        }

        suspend fun getPosts(latitude: String, longitude: String, radio: String) : List<Post>? {
            var postsList : List<Post>? = listOf()
            try{
                postsList = httpClient.getPosts(latitude, longitude, radio, null, null, null, null, null, null, null).await()
            }catch(error : Exception){
                Log.d("error", error.message)
            }
            return postsList
        }

        suspend fun getPostsById(id:String) : List<Post>? {
            var postsList : List<Post>? = null
            try{
                postsList = httpClient.getPostsById(id).await()
            }catch(error : Exception){
                Log.d("error", error.message)
            }
            return postsList
        }

        suspend fun getPostsInterested(idUser:String, idPost:String) : List<User>? {
            var userList : List<User>? = null
            try{
                userList = httpClient.getPostsInterested(idUser, idPost).await()
            }catch(error : Exception){
                Log.d("error", error.message)
            }
            return userList
        }

        suspend fun getPostsInterestedIn(id:String) : List<Post>? {
            var postsList : List<Post>? = null
            try{
                postsList = httpClient.getPostsInterestedIn(id).await()
            }catch(error : Exception){
                Log.d("error", error.message)
            }
            return postsList
        }

        suspend fun getPostsAdoptedById(id:String) : List<Post>? {
            var postsList : List<Post>? = null
            try{
                postsList = httpClient.getPostsAdoptedById(id).await()
            }catch(error : Exception){
                Log.d("error", error.message)
            }
            return postsList
        }

        suspend fun getMonitoring(id: String) : Monitoring? {
            var monitoring : Monitoring? = null
            try{
                monitoring = httpClient.getMonitoring(id).await()
            }catch(error : Exception){
                Log.d("error", error.message)
            }
            return monitoring
        }

        suspend fun updateMonitoring(monitoring: Monitoring) : String{
            var message: String? = null
            try {
                message = httpClient.updateMonitoring(monitoring, monitoring.postId).await()
            }catch (error: Exception){
                message = error.message
            }
            return message!!
        }

        suspend fun addFormAdoption(formAdoption: FormAdoption, postId : String): String {
            var messageRet: String = ""
            try {
                httpClient.addFormAdoption(formAdoption, postId).await()
                messageRet = "Formulario Enviado"
            } catch (error: Exception) {
                messageRet = error.message.toString()
            }
            return messageRet
        }

        suspend fun getFormAdoption(userId: String, postId: String) : FormAdoption {
            var formAdoption : FormAdoption? = null
            try{
                formAdoption = httpClient.getFormAdoption(userId, postId).await()
            }catch(error: Exception){

            }
            return formAdoption!!
        }

        suspend fun confirmAdoption(adoptionConfirm: AdoptionConfirm): String {
            var msgResponse: String? = null
            try{
                httpClient.confirmAdoption(adoptionConfirm).await()
                msgResponse = "Adopción confirmada exitosamente"
            }catch(error: Exception){
                msgResponse = error.message

            }
            return msgResponse!!
        }

       suspend fun deleteInterested(userId: String, postId: String): String {
           var msgResponse: String? = null
           try{
               httpClient.deleteInterested(userId, postId).await()
               msgResponse = "Adoptante eliminado exitosamente."
           }catch(error: Exception){
               msgResponse = error.message

           }
           return msgResponse!!
       }

        suspend fun updatePost(post: PostCreate, postId : String): String {
            var messageRet: String = ""
            try {
                val response = httpClient.updatePost(post, postId).awaitResponse()
                messageRet = "Publicacion modificada"
            } catch (error: Exception) {
                messageRet = error.message.toString()
            }
            return messageRet
        }

        suspend fun addPost(filesPath: List<MultipartBody.Part?>, post: PostCreate): String {
            var messageRet: String = ""
            var userId = toRequestBody(post.userId)
            val petName = toRequestBody(post.petName)
            val petSex = toRequestBody(post.petSex)
            val petType = toRequestBody(post.petType)
            val postType = toRequestBody(post.postType)
            val petBreed = toRequestBody(post.petBreed)
            val petAge = toRequestBody(post.petAge)
            val petSize = toRequestBody(post.petSize)
            val petWeigth = toRequestBody(post.petWeigth)
            val petCastrated = toRequestBody(post.petCastrated)
            val postDescription = toRequestBody(post.postDescription)
            val petMonitoring = toRequestBody(post.petMonitoring)
            val latitude = toRequestBody(post.latitude.toString())
            val longitude = toRequestBody(post.longitude.toString())
            val postTitle = toRequestBody(post.postTitle)

            if(filesPath.size == 1){
               try {
                    httpClient.addPost(filesPath[0]!!, null,null,null,null,userId!!, petName!!, petSex!!, petType!!, postType!!, petBreed!!, petAge!!,
                        petSize!!, petWeigth!!, petCastrated!!, postDescription!!, petMonitoring!!, latitude!!, longitude!!, postTitle!!).awaitResponse()
                    messageRet = "Publicacion creada exitosamente"
                } catch (error: Exception) {
                    messageRet = error.message.toString()
                }
            }else if(filesPath.size == 2){
                try {
                    httpClient.addPost(filesPath[0]!!, filesPath[1],null,null,null,userId!!, petName!!, petSex!!, petType!!, postType!!, petBreed!!, petAge!!,
                        petSize!!, petWeigth!!, petCastrated!!, postDescription!!, petMonitoring!!, latitude!!, longitude!!, postTitle!!).awaitResponse()
                    messageRet = "Publicacion creada exitosamente"
                } catch (error: Exception) {
                    messageRet = error.message.toString()
                }
            }else if(filesPath.size == 3){
                try {
                    httpClient.addPost(filesPath[0]!!, filesPath[1],filesPath[2],null,null,userId!!, petName!!, petSex!!, petType!!, postType!!, petBreed!!, petAge!!,
                        petSize!!, petWeigth!!, petCastrated!!, postDescription!!, petMonitoring!!, latitude!!, longitude!!, postTitle!!).awaitResponse()
                    messageRet = "Publicacion creada exitosamente"
                } catch (error: Exception) {
                    messageRet = error.message.toString()
                }
            }else if(filesPath.size == 4){
                try {
                    httpClient.addPost(filesPath[0]!!, filesPath[1],filesPath[2],filesPath[3],null,userId!!, petName!!, petSex!!, petType!!, postType!!, petBreed!!, petAge!!,
                        petSize!!, petWeigth!!, petCastrated!!, postDescription!!, petMonitoring!!, latitude!!, longitude!!, postTitle!!).awaitResponse()
                    messageRet = "Publicacion creada exitosamente"
                } catch (error: Exception) {
                    messageRet = error.message.toString()
                }
            }else if(filesPath.size == 5){
                try {
                    httpClient.addPost(filesPath[0]!!, filesPath[1],filesPath[2],filesPath[3],filesPath[4],userId!!, petName!!, petSex!!, petType!!, postType!!, petBreed!!, petAge!!,
                        petSize!!, petWeigth!!, petCastrated!!, postDescription!!, petMonitoring!!, latitude!!, longitude!!, postTitle!!).awaitResponse()
                    messageRet = "Publicacion creada exitosamente"
                } catch (error: Exception) {
                    messageRet = error.message.toString()
                }
            }

            return messageRet
        }

    }
}


fun toRequestBody(value: String?): RequestBody? {
    return RequestBody.create("text/plain".toMediaTypeOrNull(), value!!)
}