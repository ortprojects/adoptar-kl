package com.adoptar.adoptar.modules.posts.domain

data class PostCreate(
    val userId: String,
    val petName: String,
    val petSex: String,
    val petType: String,
    val postType: String,
    val petBreed: String,
    val petAge: String,
    val petSize: String,
    val petWeigth: String,
    val petCastrated: String,
    val postDescription: String,
    val petMonitoring: String,
    val latitude: Double,
    val longitude: Double,
    val postTitle: String,
)