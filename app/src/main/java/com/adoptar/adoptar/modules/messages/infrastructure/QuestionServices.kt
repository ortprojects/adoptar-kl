package com.adoptar.adoptar.modules.messages.infrastructure

import com.adoptar.adoptar.modules.messages.domain.AnswerPost
import com.adoptar.adoptar.modules.messages.domain.Question
import com.adoptar.adoptar.modules.messages.domain.QuestionPost
import com.adoptar.adoptar.modules.shared.http.HttpClient
import retrofit2.await
import retrofit2.awaitResponse
import java.lang.Exception

class QuestionServices {
    companion object {
        val httpClient = HttpClient.getHttpClient().create(QuestionRepository::class.java)

        suspend fun getQuestionByPost(id: String): MutableList<Question>? {
            var questionRet: MutableList<Question>? = null
            try {
                questionRet = httpClient.getQuestionByPost(id).await()
                return questionRet
            } catch (error: Exception) {
                return questionRet
            }
        }

        suspend fun questionPost(question: QuestionPost, idPost : String): String {
            var messageRet: String = ""
            try {
                val response = httpClient.questionPost(question, idPost).awaitResponse()
                messageRet = "Pregunta agregado"
            } catch (error: Exception) {
                messageRet = error.message.toString()
            }
            return messageRet
        }

        suspend fun answerToQuestionPost(answer: AnswerPost, idPost:String, idQuestion:String): String {
            var messageRet: String = ""
            try {
                val response = httpClient.answerToQuestionPost(answer, idPost, idQuestion).awaitResponse()
                messageRet = "Respuesta agregada."

            } catch (error: Exception) {
                messageRet = error.message.toString()
            }
            return messageRet
        }


    }
}