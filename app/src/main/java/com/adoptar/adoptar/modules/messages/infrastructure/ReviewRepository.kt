package com.adoptar.adoptar.modules.messages.infrastructure

import com.adoptar.adoptar.modules.messages.domain.*
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface ReviewRepository {
    @GET("reviews/{id}")
    fun getReviewsByUserId(@Path("id") id:String): retrofit2.Call<MutableList<Review>>

    @POST("reviews")
    fun reviewPost(@Body review : ReviewPost) : retrofit2.Call<Void>

    @POST("reviews/{idCreatedBy}/{idReview}/response")
    fun responseToReviewPost(@Body response : ReviewResponsePost, @Path("idCreatedBy") idCreatedBy:String, @Path("idReview") idReview:String): retrofit2.Call<Void>
}