package com.adoptar.adoptar.modules.posts.domain

import android.os.Build
import android.os.Parcel
import android.os.Parcelable
import androidx.annotation.RequiresApi
import com.adoptar.adoptar.modules.users.domain.User
import java.util.ArrayList

data class Post(
    val id: String?,
    val imageFiles: ArrayList<String>?,
    val latitude: Double,
    val longitude: Double,
    val petAge: String?,
    val petBreed: String?,
    val petCastrated: String?,
    val petName: String?,
    val title: String?,
    val petSex: String?,
    val petSize: String?,
    val petType: String?,
    val petWeigth: String?,
    val createdAt: String?,
    val postDescription: String?,
    val postType: String?,
    val userId: String?,
    var timeAgo: String?,
    var createdBy: User?,
    var adoptedBy: User?,
    var petMonitoring: Boolean
) : Parcelable {
    @RequiresApi(Build.VERSION_CODES.Q)
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.createStringArrayList(),
        parcel.readDouble(),
        parcel.readDouble(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readParcelable(User::class.java.classLoader),
        parcel.readParcelable(User::class.java.classLoader),
        parcel.readBoolean()
    ) {
    }

    @RequiresApi(Build.VERSION_CODES.Q)
    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeStringList(imageFiles)
        parcel.writeDouble(latitude)
        parcel.writeDouble(longitude)
        parcel.writeString(petAge)
        parcel.writeString(petBreed)
        parcel.writeString(petCastrated)
        parcel.writeString(petName)
        parcel.writeString(title)
        parcel.writeString(petSex)
        parcel.writeString(petSize)
        parcel.writeString(petType)
        parcel.writeString(petWeigth)
        parcel.writeString(createdAt)
        parcel.writeString(postDescription)
        parcel.writeString(postType)
        parcel.writeString(userId)
        parcel.writeString(timeAgo)
        parcel.writeParcelable(createdBy, flags)
        parcel.writeParcelable(adoptedBy, flags)
        parcel.writeBoolean(petMonitoring)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Post> {
        @RequiresApi(Build.VERSION_CODES.Q)
        override fun createFromParcel(parcel: Parcel): Post {
            return Post(parcel)
        }

        override fun newArray(size: Int): Array<Post?> {
            return arrayOfNulls(size)
        }
    }

}