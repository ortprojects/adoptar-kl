package com.adoptar.adoptar.modules.notifications.infraestructure

import com.adoptar.adoptar.modules.notifications.domain.Notification
import com.adoptar.adoptar.modules.notifications.domain.NotificationIsRead
import retrofit2.http.*

interface NotificationRepository {

    @GET("notifications?")
    fun getNotificationsById(@Query("userId") userId: String): retrofit2.Call<MutableList<Notification>>

    @DELETE("notifications/{id}")
    fun deleteNotificationsById(@Path("id") id: String): retrofit2.Call<Void>

    @PUT("notifications/{id}")
    fun updateNotificationsById(
        @Body isRead: NotificationIsRead,
        @Path("id") id: String
    ): retrofit2.Call<Void>
}