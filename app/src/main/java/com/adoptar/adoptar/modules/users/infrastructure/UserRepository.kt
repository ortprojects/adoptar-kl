package com.adoptar.adoptar.modules.users.infrastructure

import com.adoptar.adoptar.modules.users.domain.User
import com.adoptar.adoptar.modules.users.domain.UserSimply
import retrofit2.http.*

interface UserRepository {

    @GET("users/{id}")
    fun userById(@Path("id") id:String): retrofit2.Call<User>

    @POST("users")
    fun userPost(@Body user : UserSimply): retrofit2.Call<Void>

    @PUT("users")
    fun userPut(@Body user: User): retrofit2.Call<Void>

}