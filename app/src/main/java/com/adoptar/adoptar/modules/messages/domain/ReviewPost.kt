package com.adoptar.adoptar.modules.messages.domain

import android.os.Parcel
import android.os.Parcelable

data class ReviewPost (
    val createdByUserId: String?,
    val userId: String?,
    val text: String?
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(createdByUserId)
        parcel.writeString(userId)
        parcel.writeString(text)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ReviewPost> {
        override fun createFromParcel(parcel: Parcel): ReviewPost {
            return ReviewPost(parcel)
        }

        override fun newArray(size: Int): Array<ReviewPost?> {
            return arrayOfNulls(size)
        }
    }
}