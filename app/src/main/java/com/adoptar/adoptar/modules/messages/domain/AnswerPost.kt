package com.adoptar.adoptar.modules.messages.domain

import android.os.Parcel
import android.os.Parcelable

data class AnswerPost(
    val createdByUserId: String?,
    val message: String?
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(createdByUserId)
        parcel.writeString(message)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<AnswerPost> {
        override fun createFromParcel(parcel: Parcel): AnswerPost {
            return AnswerPost(parcel)
        }

        override fun newArray(size: Int): Array<AnswerPost?> {
            return arrayOfNulls(size)
        }
    }
}