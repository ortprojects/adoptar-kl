package com.adoptar.adoptar.modules.notifications.domain

class NotificationIsRead (
    var isRead: Boolean
)