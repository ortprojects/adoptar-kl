package com.adoptar.adoptar.modules.posts.infrastructure

import com.adoptar.adoptar.modules.posts.domain.*
import com.adoptar.adoptar.modules.users.domain.User
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.*

interface PostRepository {

    // TODO: Agregar Query params con latitud, longitud y radio.
    //@GET("posts?latitude=-34.6122511&longitude=-58.4298289&radio=10")
    @GET("posts?")
    fun getPosts(@Query("latitude") latitude: String, @Query("longitude") longitude: String, @Query("radio") radio: String,
                 @Query("petMonitoring") petMonitoring: String?, @Query("petCastratedType") petCastratedType: String?, @Query("petAgeTypes") petAgeTypes: String?,
                 @Query("petSexType") petSexType: String?, @Query("petSizeType") petSizeType: String?, @Query("petType") petType: String?,
                 @Query("postType") postType: String?): retrofit2.Call<MutableList<Post>>

    @GET("users/{id}/posts")
    fun getPostsById(@Path("id") id:String ): retrofit2.Call<MutableList<Post>>

    @GET("users/{idUser}/interested/{idPost}")
    fun getPostsInterested(@Path("idUser") idUser:String , @Path("idPost") idPost:String ): retrofit2.Call<MutableList<User>>

    @GET("users/{id}/interested-in")
    fun getPostsInterestedIn(@Path("id") id:String ): retrofit2.Call<MutableList<Post>>

    @GET("users/{id}/adopted")
    fun getPostsAdoptedById(@Path("id") id:String ): retrofit2.Call<MutableList<Post>>

    @GET("posts/{postId}/form-adoption/{userId}")
    fun getFormAdoption(@Path("userId") userId: String, @Path("postId") postId: String): retrofit2.Call<FormAdoption>

    @GET("monitorings")
    fun getMonitoring(@Query("postId") postId: String): retrofit2.Call<Monitoring>

    @POST("posts/{id}/form-adoption")
    fun addFormAdoption(@Body form : FormAdoption, @Path("id") PostId:String): retrofit2.Call<Void>

    @POST("adoptions")
    fun confirmAdoption(@Body adoptionConfirm: AdoptionConfirm) : retrofit2.Call<Void>

    @DELETE("users/{userId}/interested/{postId}")
    fun deleteInterested(@Path("userId") userId: String, @Path("postId") postId: String): retrofit2.Call<Void>

    @Multipart
    @POST("posts")
    fun addPost(@Part imagesFiles: MultipartBody.Part, @Part imagesFiles2: MultipartBody.Part?, @Part imagesFiles3: MultipartBody.Part?,@Part imagesFiles4: MultipartBody.Part?,
                @Part imagesFiles5: MultipartBody.Part?, @Part("userId") userId: RequestBody, @Part("petName") petName: RequestBody,
                @Part("petSex") petSex: RequestBody,@Part("petType") petType: RequestBody,@Part("postType") postType: RequestBody,
                @Part("petBreed") petBreed: RequestBody,@Part("petAge") petAge: RequestBody,@Part("petSize") petSize: RequestBody,
                @Part("petWeigth") petWeigth: RequestBody,@Part("petCastrated") petCastrated: RequestBody,@Part("postDescription") postDescription: RequestBody,
                @Part("petMonitoring") petMonitoring: RequestBody,@Part("latitude") latitude: RequestBody,@Part("longitude") longitude: RequestBody,
                @Part("postTitle") postTitle: RequestBody): retrofit2.Call<Void>



    @PUT("posts/{postId}")
    fun updatePost(@Body post: PostCreate, @Path("postId") postId : String) : retrofit2.Call<Void>


    @PUT("monitorings/{monitoringId}")
    fun updateMonitoring(@Body monitoring: Monitoring, @Path("monitoringId") monitoringId : String) : retrofit2.Call<String>



}