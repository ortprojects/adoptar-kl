package com.adoptar.adoptar.modules.messages.infrastructure

import com.adoptar.adoptar.modules.messages.domain.*
import com.adoptar.adoptar.modules.shared.http.HttpClient
import retrofit2.await
import retrofit2.awaitResponse
import java.lang.Exception

class ReviewServices {
    companion object {
        val httpClient = HttpClient.getHttpClient().create(ReviewRepository::class.java)

        suspend fun getReviewsByUserId(id: String): MutableList<Review>? {
            var reviewRet: MutableList<Review>? = null
            try {
                reviewRet = httpClient.getReviewsByUserId(id).await()
                return reviewRet
            } catch (error: Exception) {
                return reviewRet
            }
        }

        suspend fun reviewPost(review: ReviewPost): String {
            var messageRet: String = ""
            try {
                val response = httpClient.reviewPost(review).awaitResponse()
                messageRet = "Reseña agregada"
            } catch (error: Exception) {
                messageRet = error.message.toString()
            }
            return messageRet
        }

        suspend fun responseToReviewPost(response: ReviewResponsePost, idCreatedBy:String, idReview:String): String {
            var messageRet: String = ""
            try {
                val response = httpClient.responseToReviewPost(response, idCreatedBy, idReview).awaitResponse()
                messageRet = "Respuesta agregada."

            } catch (error: Exception) {
                messageRet = error.message.toString()
            }
            return messageRet
        }


    }
}