package com.adoptar.adoptar.modules.posts.domain

data class AdoptionConfirm(
    val postId: String,
    val userIdSelected: String
)