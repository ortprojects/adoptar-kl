package com.adoptar.adoptar.modules.messages.domain

import android.os.Parcel
import android.os.Parcelable
import com.adoptar.adoptar.modules.users.domain.User

data class Review(
    val createdBy: User?,
    val id: String?,
    val response: Response?,
    val text: String?,
    val userId: String?
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readParcelable(User::class.java.classLoader),
        parcel.readString(),
        parcel.readParcelable(Response::class.java.classLoader),
        parcel.readString(),
        parcel.readString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeParcelable(createdBy, flags)
        parcel.writeString(id)
        parcel.writeParcelable(response, flags)
        parcel.writeString(text)
        parcel.writeString(userId)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Review> {
        override fun createFromParcel(parcel: Parcel): Review {
            return Review(parcel)
        }

        override fun newArray(size: Int): Array<Review?> {
            return arrayOfNulls(size)
        }
    }
}