package com.adoptar.adoptar.modules.posts.domain

data class Monitoring(
    var daysControlChecked: Boolean,
    var isFinished: Boolean,
    var newHouserChecked: Boolean,
    var petPhotoChecked: Boolean,
    val postId: String,
    var sanitaryNotebookChecked: Boolean
)