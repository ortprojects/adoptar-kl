package com.adoptar.adoptar.modules.notifications.infraestructure

import android.util.Log
import com.adoptar.adoptar.modules.notifications.domain.Notification
import com.adoptar.adoptar.modules.notifications.domain.NotificationIsRead
import com.adoptar.adoptar.modules.posts.infrastructure.PostService
import com.adoptar.adoptar.modules.shared.http.HttpClient
import retrofit2.await
import retrofit2.awaitResponse
import java.lang.Exception

class NotificationService {

    companion object {
        val httpClient = HttpClient.getHttpClient().create(NotificationRepository::class.java)

        suspend fun getNotificationsById(userId: String): MutableList<Notification>? {
            var notificationList: MutableList<Notification>? = null
            try {
                notificationList = httpClient.getNotificationsById(userId).await()
            } catch (error: Exception) {
                Log.d("error", error.message)
            }
            return notificationList
        }

        suspend fun deleteNotificationsById(id: String): String {
            try {
                httpClient.deleteNotificationsById(id).await()
                return "Notificación borrada"
            } catch (error: Exception) {
                Log.d("error", error.message)
                return "Ocurrió un error, intente de nuevo mas tarde"
            }
        }

        suspend fun updateNotificationsById(isRead: NotificationIsRead, id: String): String {
            try {
                httpClient.updateNotificationsById(isRead, id).await()
                return "Publicacion modificada"
            } catch (error: Exception) {
                Log.d("error", error.message)
                return "Ocurrio un error, intente de nuevo mas tarde"
            }
        }

    }
}