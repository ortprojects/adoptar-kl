package com.adoptar.adoptar.modules.messages.domain

import android.os.Parcel
import android.os.Parcelable

data class QuestionPost(
    val createdByUserId: String?,
    val question: String?
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(createdByUserId)
        parcel.writeString(question)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<QuestionPost> {
        override fun createFromParcel(parcel: Parcel): QuestionPost {
            return QuestionPost(parcel)
        }

        override fun newArray(size: Int): Array<QuestionPost?> {
            return arrayOfNulls(size)
        }
    }
}