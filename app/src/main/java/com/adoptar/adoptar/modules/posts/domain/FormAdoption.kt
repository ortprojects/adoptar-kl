package com.adoptar.adoptar.modules.posts.domain

import android.os.Parcel
import android.os.Parcelable

data class FormAdoption(
    val aboutPeople: String?,
    val aboutPets: String?,
    val aboutPetCastrated: String?,
    val locationType: String?,
    val hasNoPet: String?,
    val createdByUserId: String?


): Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(aboutPeople)
        parcel.writeString(aboutPets)
        parcel.writeString(aboutPetCastrated)
        parcel.writeString(locationType)
        parcel.writeString(hasNoPet)
        parcel.writeString(createdByUserId)

    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<FormAdoption> {
        override fun createFromParcel(parcel: Parcel): FormAdoption {
            return FormAdoption(parcel)
        }

        override fun newArray(size: Int): Array<FormAdoption?> {
            return arrayOfNulls(size)
        }
    }
}