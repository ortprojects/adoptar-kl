package com.adoptar.adoptar.modules.messages.domain

import android.os.Parcel
import android.os.Parcelable
import com.adoptar.adoptar.modules.users.domain.User

data class Question(
    val answer: Answer?,
    val createdAt: String?,
    val createdBy: User?,
    val id: String?,
    val text: String?
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readParcelable(Answer::class.java.classLoader),
        parcel.readString(),
        parcel.readParcelable(User::class.java.classLoader),
        parcel.readString(),
        parcel.readString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeParcelable(answer, flags)
        parcel.writeString(createdAt)
        parcel.writeParcelable(createdBy, flags)
        parcel.writeString(id)
        parcel.writeString(text)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Question> {
        override fun createFromParcel(parcel: Parcel): Question {
            return Question(parcel)
        }

        override fun newArray(size: Int): Array<Question?> {
            return arrayOfNulls(size)
        }
    }
}