package com.adoptar.adoptar.modules.users.infrastructure

import android.util.Log
import android.view.View
import com.adoptar.adoptar.modules.shared.http.HttpClient
import com.adoptar.adoptar.modules.users.domain.User
import com.adoptar.adoptar.modules.users.domain.UserSimply
import kotlinx.coroutines.*
import retrofit2.await
import retrofit2.awaitResponse
import java.lang.Exception

class UserServices {
    companion object {
        val httpClient = HttpClient.getHttpClient().create(UserRepository::class.java)

        suspend fun userById(id: String): User? {
            var userRet: User? = null
            try {
                userRet = httpClient.userById(id).await()
                return userRet
            } catch (error: Exception) {
                return userRet
            }
        }

        suspend fun userPost(user: UserSimply): String {
            var messageRet: String = ""
            try {
                val response = httpClient.userPost(user).awaitResponse()
                messageRet = "Usuario agregado"
            } catch (error: Exception) {
                messageRet = error.message.toString()
            }
            return messageRet
        }

        suspend fun userPut(user: User): String {
            var messageRet: String = ""
            try {
                val response = httpClient.userPut(user).awaitResponse()
                messageRet = "Usuario modificado."

            } catch (error: Exception) {
                messageRet = error.message.toString()
            }
            return messageRet
        }


    }
}