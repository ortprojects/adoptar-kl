package com.adoptar.adoptar.modules.messages.infrastructure

import com.adoptar.adoptar.modules.messages.domain.AnswerPost
import com.adoptar.adoptar.modules.messages.domain.Question
import com.adoptar.adoptar.modules.messages.domain.QuestionPost
import com.adoptar.adoptar.modules.users.domain.User
import retrofit2.http.*

interface QuestionRepository {
    @GET("posts/{id}/questions-answers")
    fun getQuestionByPost(@Path("id") id:String): retrofit2.Call<MutableList<Question>>

    @POST("posts/{id}/questions-answers")
    fun questionPost(@Body question : QuestionPost, @Path ("id") id:String): retrofit2.Call<Void>

    @POST("posts/{idPost}/questions-answers/{idQuestion}/answer")
    fun answerToQuestionPost(@Body answer : AnswerPost, @Path ("idPost") idPost:String, @Path ("idQuestion") idQuestion:String): retrofit2.Call<Void>

}