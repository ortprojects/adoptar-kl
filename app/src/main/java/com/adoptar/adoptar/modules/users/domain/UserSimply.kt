package com.adoptar.adoptar.modules.users.domain

data class UserSimply(
    val email: String,
    val id: String
)