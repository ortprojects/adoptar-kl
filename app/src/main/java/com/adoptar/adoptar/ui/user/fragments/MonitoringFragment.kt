package com.adoptar.adoptar.ui.user.fragments

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.CheckBox
import android.widget.Toast
import com.adoptar.adoptar.R
import com.adoptar.adoptar.modules.posts.domain.Monitoring
import com.adoptar.adoptar.modules.posts.infrastructure.PostService
import com.adoptar.adoptar.ui.posts.fragments.PostDetailFragmentArgs
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class MonitoringFragment : Fragment() {
    lateinit var monitoring: Monitoring

    lateinit var daysControlChecked: CheckBox
    lateinit var isFinished: CheckBox
    lateinit var newHouserChecked: CheckBox
    lateinit var petPhotoChecked: CheckBox
    lateinit var sanitaryNotebookChecked: CheckBox
    lateinit var btnSave: Button
    lateinit var v: View


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val postId = MonitoringFragmentArgs.fromBundle(requireArguments()).postId
        GlobalScope.launch(Dispatchers.Main) {
            try {
                monitoring = PostService.getMonitoring(postId!!)!!
                Log.i("Post id es: ", "$monitoring" )
                daysControlChecked.isChecked = monitoring.daysControlChecked
                isFinished.isChecked = monitoring.isFinished
                newHouserChecked.isChecked = monitoring.newHouserChecked
                petPhotoChecked.isChecked = monitoring.petPhotoChecked
                sanitaryNotebookChecked.isChecked = monitoring.sanitaryNotebookChecked


            } catch (e: Exception) {
                Log.i("El error es: ", "$e")
            }
        }

        btnSave = v.findViewById(R.id.btnSave)
        btnSave.setOnClickListener {
            var message: String? = null
            monitoring.petPhotoChecked = petPhotoChecked.isChecked
            monitoring.daysControlChecked = daysControlChecked.isChecked
            monitoring.isFinished = isFinished.isChecked
            monitoring.newHouserChecked = newHouserChecked.isChecked
            monitoring.sanitaryNotebookChecked = sanitaryNotebookChecked.isChecked
            GlobalScope.launch(Dispatchers.Main){
                try{
                    PostService.updateMonitoring(monitoring)
                    Toast.makeText(v.context, "Monitoreo modificado exitosamente.", Toast.LENGTH_LONG).show()
                }catch(error: Exception){
                    Toast.makeText(v.context, error.message, Toast.LENGTH_LONG).show()
                }
            }
        }
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        v = inflater.inflate(R.layout.fragment_monitoring, container, false)
        daysControlChecked = v.findViewById(R.id.daysControlChecked)
        isFinished = v.findViewById(R.id.isFinished)
        newHouserChecked = v.findViewById(R.id.newHouserChecked)
        petPhotoChecked = v.findViewById(R.id.petPhotoChecked)
        sanitaryNotebookChecked = v.findViewById(R.id.sanitaryNotebookChecked)
        return v
    }

    companion object {
        fun newInstance() = MonitoringFragment()
    }
}




/*
{
    "petPhotoChecked": true,
    "newHouserChecked": true,
    "sanitaryNotebookChecked": false,
    "daysControlChecked": false,
    "isFinished": false,
    "postId": "d446974d-0a8d-44bc-b23f-6a306cb8feb0"
}*/