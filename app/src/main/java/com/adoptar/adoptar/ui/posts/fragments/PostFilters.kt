package com.adoptar.adoptar.ui.posts.fragments

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Color
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.getSystemService
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.adoptar.adoptar.R
import com.adoptar.adoptar.modules.posts.domain.Post
import com.adoptar.adoptar.modules.posts.infrastructure.PostService
import com.adoptar.adoptar.ui.posts.adapter.PostsAdapter
import com.adoptar.adoptar.ui.posts.viewModels.PostsViewModel
import com.google.android.gms.common.api.Status
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.api.net.PlacesClient
import com.google.android.libraries.places.widget.AutocompleteSupportFragment
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.util.*
import kotlin.collections.ArrayList

class PostFilters : Fragment() {
    lateinit var v: View
    lateinit var placesClient: PlacesClient
    var address: Place? = null

    lateinit var chbPuppy: CheckBox
    lateinit var chbYoung: CheckBox
    lateinit var chbAdult: CheckBox
    lateinit var chbVeteran: CheckBox
    lateinit var barDistance: SeekBar
    lateinit var btnSend: Button
    lateinit var btnClean: Button
    lateinit var barText: TextView
    lateinit var radAddress: RadioButton
    lateinit var radCurrentAddress: RadioButton
    lateinit var radioGroupType: RadioGroup
    lateinit var radioGroupSize: RadioGroup
    lateinit var radioGroupCastrated: RadioGroup
    lateinit var radioGroupSex: RadioGroup
    lateinit var radioGroupTracing: RadioGroup
    lateinit var radioGroupTypePet: RadioGroup
    lateinit var postsFilter: List<Post>
    lateinit var currentLocation: Location
    lateinit var latitude: String
    lateinit var longitude: String


    private val postViewModel: PostsViewModel by activityViewModels()


    var placeFields = Arrays.asList(
        Place.Field.ID,
        Place.Field.NAME,
        Place.Field.ADDRESS,
        Place.Field.LAT_LNG
    )

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //var permissionCheck = ContextCompat.checkSelfPermission(v.context, Manifest.permission.ACCESS_FINE_LOCATION)

        if(ActivityCompat.checkSelfPermission(v.context,android.Manifest.permission.ACCESS_COARSE_LOCATION)!= PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(v.context,android.Manifest.permission.ACCESS_FINE_LOCATION)!= PackageManager.PERMISSION_GRANTED) {

            requestPermissions(arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION,android.Manifest.permission.ACCESS_COARSE_LOCATION),200)


        }else{
            var locationManager = requireActivity().getSystemService(Context.LOCATION_SERVICE) as LocationManager

            currentLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_post_filters, container, false)

        radAddress = v.findViewById(R.id.radAddress)
        radCurrentAddress = v.findViewById(R.id.radCurrentAddress)
        radioGroupType = v.findViewById(R.id.radioGroupType)
        radioGroupSize = v.findViewById(R.id.radioGroupSize)
        radioGroupCastrated = v.findViewById(R.id.radioGroupCastrated)
        radioGroupSex = v.findViewById(R.id.radioGroupSex)
        radioGroupTracing = v.findViewById(R.id.radioGroupTracing)
        radioGroupTypePet = v.findViewById(R.id.radioGroupTypePet)
        chbPuppy = v.findViewById(R.id.chbPuppy)
        chbYoung = v.findViewById(R.id.chbYoung)
        chbAdult = v.findViewById(R.id.chbAdult)
        chbVeteran = v.findViewById(R.id.chbVeteran)
        barDistance = v.findViewById(R.id.barDistance)
        btnSend = v.findViewById(R.id.btnSend)
        btnClean = v.findViewById(R.id.btnClean)
        barText = v.findViewById(R.id.textView6)
        barText.text = barDistance.progress.toString()

        btnClean.setOnClickListener {
            cleanFilter()
        }

        btnSend.setOnClickListener() {
            applyFilter()
        }

        //TODO MODULARIZAR ESTO
        val autocompleteFragment = childFragmentManager
            .findFragmentById(R.id.autocomplete_fragment_filter) as AutocompleteSupportFragment

        radAddress.setOnClickListener() {
            autocompleteFragment.view?.visibility = View.VISIBLE
        }

        radCurrentAddress.setOnClickListener() {
            autocompleteFragment.view?.visibility = View.GONE
            Log.d("latitudeeeeeeeeeeeeee", currentLocation.latitude.toString())
            Log.d("longitudeeeeeeeeeeeeee", currentLocation.longitude.toString())

        }

        changeBarProgress()

        return v
    }

    override fun onStart() {
        super.onStart()

        initPlaces()
        setupPlacesAutocomplete()


    }



    private fun changeBarProgress(){
        barDistance.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {


            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                barText.text = barDistance.progress.toString()
            }


            override fun onStartTrackingTouch(seekBar: SeekBar) {

            }

            override fun onStopTrackingTouch(seekBar: SeekBar) {

            }
        })
    }

    private fun setupPlacesAutocomplete() {
        val autocompleteFragment = childFragmentManager
            .findFragmentById(R.id.autocomplete_fragment_filter) as AutocompleteSupportFragment

        autocompleteFragment.view?.visibility = View.GONE
        autocompleteFragment.setPlaceFields(placeFields)
        autocompleteFragment.setHint("Buscar")
        val etTextInput: EditText? =
            autocompleteFragment.view?.findViewById(R.id.places_autocomplete_search_input)
        if (etTextInput != null) {
            etTextInput.setTextColor(Color.WHITE)
        }

        autocompleteFragment.setOnPlaceSelectedListener(object : PlaceSelectionListener {
            override fun onPlaceSelected(p0: Place) {
                address = p0
                latitude = p0.latLng!!.latitude.toString()
                longitude = p0.latLng!!.latitude.toString()
            }

            override fun onError(p0: Status) {
            }
        })
        autocompleteFragment.view?.findViewById<View>(R.id.places_autocomplete_clear_button)
            ?.setOnClickListener() {
                address = null
                autocompleteFragment.setText("")
            }
    }

    private fun initPlaces() {
        Places.initialize(v.context, "AIzaSyCFg3S_ycV418XGqN1FctIsT5S02XXqdcE")
        placesClient = Places.createClient(v.context)
    }

    private fun cleanFilter() {
        radioGroupSex.clearCheck()
        radioGroupSize.clearCheck()
        radioGroupCastrated.clearCheck()
        radioGroupTracing.clearCheck()
        radioGroupType.clearCheck()
        radioGroupTypePet.clearCheck()
        chbPuppy.isChecked = false
        chbYoung.isChecked = false
        chbAdult.isChecked = false
        chbVeteran.isChecked = false
        radCurrentAddress.isChecked = false
        radAddress.isChecked = false

        address = null
        val autocompleteFragment = childFragmentManager
            .findFragmentById(R.id.autocomplete_fragment_filter) as AutocompleteSupportFragment
        autocompleteFragment.view?.visibility = View.GONE
        postViewModel.getPosts(currentLocation.latitude.toString(), currentLocation.longitude.toString(), "1")
        val action = PostFiltersDirections.actionPostFiltersToNavigationHome(true)
        v.findNavController().navigate(action)
    }

    private fun applyFilter() {
        var petSex: String? = null
        var petSize: String? = null
        var petCastrated: String? = null
        var petMonitoring: String? = null
        var postType: String? = null
        var petType: String? = null
        var latAux = currentLocation.latitude.toString()
        var lonAux = currentLocation.longitude.toString()

        if(radioGroupSex.checkedRadioButtonId != -1){
            petSex = v.findViewById<RadioButton>(radioGroupSex.checkedRadioButtonId)?.text.toString()
        }
        if(radioGroupSize.checkedRadioButtonId != -1){
            petSize = v.findViewById<RadioButton>(radioGroupSize.checkedRadioButtonId)?.text.toString()
        }
        if(radioGroupCastrated.checkedRadioButtonId != -1){
            petCastrated = v.findViewById<RadioButton>(radioGroupCastrated.checkedRadioButtonId)?.text.toString()
        }
        if(radioGroupTracing.checkedRadioButtonId != -1){
            petMonitoring = v.findViewById<RadioButton>(radioGroupTracing.checkedRadioButtonId)?.text.toString()
        }
        if(radioGroupType.checkedRadioButtonId != -1){
            postType = v.findViewById<RadioButton>(radioGroupType.checkedRadioButtonId)?.text.toString()
        }
        if(radioGroupTypePet.checkedRadioButtonId != -1){
            petType = v.findViewById<RadioButton>(radioGroupTypePet.checkedRadioButtonId)?.text.toString()
        }

        if(radAddress.isChecked){
            latAux = address?.latLng?.latitude.toString()
            lonAux = address?.latLng?.longitude.toString()
        }

        postViewModel.getPosts(latAux, lonAux, barDistance.progress.toString(), petMonitoring, petCastrated, null, petSex, petSize, petType, postType)

        val action = PostFiltersDirections.actionPostFiltersToNavigationHome(true)
        v.findNavController().navigate(action)


        //TODO ENVIAR PARAMETROS AL ENDPOINT
    }
}