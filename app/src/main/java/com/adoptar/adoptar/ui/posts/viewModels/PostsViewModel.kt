package com.adoptar.adoptar.ui.posts.viewModels

import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.adoptar.adoptar.modules.posts.domain.Post
import com.adoptar.adoptar.modules.posts.infrastructure.PostService
import com.adoptar.adoptar.ui.posts.fragments.PostsFragment
import com.adoptar.adoptar.utils.TimeAgo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.time.LocalDateTime
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter

class PostsViewModel : ViewModel() {
    val posts = MutableLiveData<List<Post>>()
    var isFirstTime = MutableLiveData<Boolean>().apply { postValue(true)}
    var isLoading = MutableLiveData<Boolean>().apply { postValue(true)}
    var isEmptyResult = MutableLiveData<Boolean>().apply { postValue(false)}

    fun getPosts(latitude: String, longitude: String, radio: String, petMonitoring: String?, petCastratedType: String?, petAgeTypes: String?,
                 petSexType: String?, petSizeType: String?, petType: String?, postType: String?) {
        isLoading.value = true
        viewModelScope.launch {
            isEmptyResult.value = false
            var postsRet = PostService.getPosts(latitude, longitude, radio, petMonitoring, petCastratedType, petAgeTypes, petSexType ,petSizeType, petType, postType)!!;
            posts.value = postsRet
            calculateTimeAgo()
            isLoading.value = false

            if (postsRet.isEmpty()){
                isEmptyResult.value = true
            }
        }
    }

    fun getPosts(latitude: String, longitude: String, radio: String,) {
        isLoading.value = true
        viewModelScope.launch {
            isEmptyResult.value = false
            var postsRet = PostService.getPosts(latitude, longitude, radio)!!;
            posts.value = postsRet
            calculateTimeAgo()
            isLoading.value = false

            if (postsRet.isEmpty()){
                isEmptyResult.value = true
            }
        }
    }

    fun setIsFirstTime(isFirsTime: Boolean){
        this.isFirstTime.value = isFirsTime
    }

    private fun calculateTimeAgo() {
        posts.value?.forEach {
            var dateOk = it.createdAt?.substringBefore(".")
            var dateParsed = LocalDateTime.parse(dateOk, DateTimeFormatter.ISO_LOCAL_DATE_TIME)
            var dateSeconds: Long = dateParsed.toEpochSecond(ZoneOffset.UTC)
            it.timeAgo = TimeAgo.getTimeAgo(dateSeconds)
        }
    }

}