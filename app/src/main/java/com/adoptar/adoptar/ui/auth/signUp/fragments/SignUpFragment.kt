package com.adoptar.adoptar.ui.auth.signUp.fragments

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.findNavController
import com.adoptar.adoptar.R
import com.adoptar.adoptar.modules.users.domain.User
import com.adoptar.adoptar.modules.users.domain.UserSimply
import com.adoptar.adoptar.modules.users.infrastructure.UserServices
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.sign_up_fragment.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class SignUpFragment : Fragment() {
    lateinit var v: View

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        v =  inflater.inflate(R.layout.sign_up_fragment, container, false)
        return v
    }

    override fun onStart() {
        super.onStart()
        setup()
    }

    private fun setup() {
        signUpButton.setOnClickListener {
            if (emailEditText.text.isNotEmpty() && passwordEditText.text.isNotEmpty() && rePasswordEditText.text.isNotEmpty()) {
                if(passwordConfirmed()){
                    FirebaseAuth.getInstance().createUserWithEmailAndPassword(
                        emailEditText.text.toString(),
                        passwordEditText.text.toString()
                    ).addOnCompleteListener {
                        if (it.isSuccessful) {
                            //TODO: enviar mail y id al back (it.getResult().user)
                            GlobalScope.launch(Dispatchers.IO) {

                                val msgResponse = UserServices.userPost(
                                    UserSimply(it.result.user?.email.toString(),it.result.user?.uid.toString())
                                )

                                withContext(Dispatchers.Main){
                                    Toast.makeText(v.context, msgResponse, Toast.LENGTH_LONG).show()
                                    val action =
                                        SignUpFragmentDirections.actionSignUpFragmentToSignInFragment()
                                    v.findNavController().navigate(action)
                                }
                            }
                        } else {
                            Log.w("TAG", "createUserWithEmail:failure", it.exception)
                            Toast.makeText(v.context, it.exception?.message ?: "Error en el servidor",
                                Toast.LENGTH_LONG).show()
                            cleanFields()
                        }
                    }
                }else{
                    Toast.makeText(v.context, "Las contraseñas no coinciden.",
                        Toast.LENGTH_LONG).show()
                    cleanFields()
                }
            }
        }
    }

    private fun passwordConfirmed(): Boolean{
        var esOk: Boolean = false
        if(passwordEditText.text.toString() == rePasswordEditText.text.toString()){
            esOk = !esOk
        }
        return esOk
    }

    private fun cleanFields(){
        emailEditText.text.clear()
        passwordEditText.text.clear()
        rePasswordEditText.text.clear()
    }
}