package com.adoptar.adoptar.ui.user.fragments

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.adoptar.adoptar.R
import com.adoptar.adoptar.ui.user.viewModels.PopupReviewViewModel

class PopupReviewFragment : Fragment() {

    companion object {
        fun newInstance() = PopupReviewFragment()
    }

    private lateinit var viewModel: PopupReviewViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.popup_review_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(PopupReviewViewModel::class.java)
        // TODO: Use the ViewModel
    }

}