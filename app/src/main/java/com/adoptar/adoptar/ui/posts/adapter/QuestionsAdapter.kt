package com.adoptar.adoptar.ui.posts.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.adoptar.adoptar.R
import com.adoptar.adoptar.modules.messages.domain.AnswerPost
import com.adoptar.adoptar.modules.messages.domain.Question
import com.adoptar.adoptar.modules.messages.infrastructure.QuestionServices
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class QuestionsAdapter(
    private var questions: MutableList<Question>,
    private val idPost: String,
    private val context: Context?,
    private val v: View,
    private val owner: Boolean,
    private val idActiveUser: String
) : RecyclerView.Adapter<QuestionsAdapter.ViewHolder>()
{
    // Provide a direct reference to each of the views within a data item
    // Used to cache the views within the item layout for fast access
    inner class ViewHolder(listItemView: View) : RecyclerView.ViewHolder(listItemView) {
        // Your holder should contain and initialize a member variable
        // for any view that will be set as you render a row
        val textQuestion:TextView?  = itemView.findViewById<TextView>(R.id.textQuestion)
        val textAnswer:TextView? = itemView.findViewById<TextView>(R.id.textAnswer)
        val editTextAnswer : EditText = itemView.findViewById<EditText>(R.id.editTextAnswer)
        val btnSubmitAnswer:Button = itemView.findViewById<Button>(R.id.btnSubmitAnswer)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)
        // Inflate the custom layout
        val questionsView = inflater.inflate(R.layout.post_question_fragment, parent, false)
        // Return a new holder instance
        return ViewHolder(questionsView)
    }

    // Involves populating data into the item through holder
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
       // Get the data model based on position
        val questionsAux: Question = questions[position]
        // Set item views based on your views and data model
        val textQuestion = viewHolder.textQuestion
        textQuestion?.text = questionsAux.text // pregunta
        val textAnswer = viewHolder.textAnswer
        val editTextAnswer = viewHolder.editTextAnswer
        val btnSubmitAnswer = viewHolder.btnSubmitAnswer

        if(!owner){
            editTextAnswer.visibility = View.GONE
            btnSubmitAnswer.visibility = View.GONE
        }

        if (questionsAux.answer != null) {
            editTextAnswer.visibility = View.GONE
            btnSubmitAnswer.visibility = View.GONE
            textAnswer?.text = questionsAux.answer.message // answer
        }
        btnSubmitAnswer.setOnClickListener {
            GlobalScope.launch(Dispatchers.Main) {
                try{
                    onItemClick(questionsAux, editTextAnswer, viewHolder)
                    Toast.makeText(v.context, "Respuesta creada exitosamente", Toast.LENGTH_SHORT).show()
                }catch(error: Exception){
                    Log.i("error: ", error.message)
                    Toast.makeText(v.context, "Error de conexión con el servidor. Intente mas tarde.", Toast.LENGTH_SHORT).show()
                }
            }
            textAnswer?.text = editTextAnswer.text.toString()
            editTextAnswer.visibility = View.GONE
            btnSubmitAnswer.visibility = View.GONE
        }

    }

    // Returns the total count of items in the list
    override fun getItemCount(): Int {
        return questions.size
    }

    suspend fun onItemClick(
        question: Question,
        editTextAnswer: EditText,
        viewHolder: ViewHolder
    ) {
        QuestionServices.answerToQuestionPost(AnswerPost(idActiveUser, editTextAnswer.text.toString()), idPost, question?.id!!)
        hideKeyboard(viewHolder)
    }

    private fun hideKeyboard(viewHolder: ViewHolder) {
        val inputManager = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputManager.hideSoftInputFromWindow(v.windowToken, 0)
    }
}