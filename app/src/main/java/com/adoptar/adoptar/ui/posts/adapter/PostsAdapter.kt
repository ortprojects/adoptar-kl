package com.adoptar.adoptar.ui.posts.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.adoptar.adoptar.R
import com.adoptar.adoptar.modules.posts.domain.Post
import com.adoptar.adoptar.ui.home.fragments.HomeFragmentDirections
import com.bumptech.glide.Glide
import com.google.android.material.card.MaterialCardView

class PostsAdapter(private val mPosts: List<Post>, val onItemClick: (Int, Boolean) -> Unit) :
    RecyclerView.Adapter<PostsAdapter.ViewHolder>() {
    // Provide a direct reference to each of the views within a data item
    // Used to cache the views within the item layout for fast access
    inner class ViewHolder(listItemView: View) : RecyclerView.ViewHolder(listItemView) {
        // Your holder should contain and initialize a member variable
        // for any view that will be set as you render a row
        val timeAgoTextView = itemView.findViewById<TextView>(R.id.card_post_timeAgo)
        val titleTextView = itemView.findViewById<TextView>(R.id.card_post_title)
        val nameTextView = itemView.findViewById<TextView>(R.id.card_post_name)
        val genderTextView = itemView.findViewById<TextView>(R.id.card_post_gender)
        val ageTextView = itemView.findViewById<TextView>(R.id.card_post_age)
        val image = itemView.findViewById<AppCompatImageView>(R.id.card_image)
        val btnEye = itemView.findViewById<ImageButton>(R.id.btnEye)

        fun getCardLayout(): MaterialCardView {
            return itemView.findViewById(R.id.card_package_item)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)
        // Inflate the custom layout
        val postView = inflater.inflate(R.layout.post_fragment, parent, false)
        // Return a new holder instance
        return ViewHolder(postView)
    }

    // Involves populating data into the item through holder
    override fun onBindViewHolder(viewHolder: PostsAdapter.ViewHolder, position: Int) {
        // Get the data model based on position
        val post: com.adoptar.adoptar.modules.posts.domain.Post = mPosts[position]
        // Set item views based on your views and data model
        val timeAgoView = viewHolder.timeAgoTextView
        timeAgoView.text = post.timeAgo
        val titleView = viewHolder.titleTextView
        titleView.text = post.title
        val nameView = viewHolder.nameTextView
        nameView.text = post.petName
        val genderView = viewHolder.genderTextView
        genderView.text = post.petSex
        val ageView = viewHolder.ageTextView
        ageView.text = post.petAge
        val media = post.imageFiles?.get(0)
        if (media !== null) {
            Glide.with(viewHolder.image.context)
                .load(media)
                .into(viewHolder.image)
        } else {
            viewHolder.image.setImageResource(R.drawable.imagotipo)
        }
        val card = viewHolder.getCardLayout()
        card.setOnClickListener() {
            onItemClick(position, true)
        }

        val btnEye = viewHolder.btnEye
        btnEye.visibility = View.GONE

        btnEye.setOnClickListener() {
            onItemClick(position, false)
        }
    }

    // Returns the total count of items in the list
    override fun getItemCount(): Int {
        return mPosts.size
    }
}