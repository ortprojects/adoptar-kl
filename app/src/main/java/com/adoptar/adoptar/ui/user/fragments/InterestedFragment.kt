package com.adoptar.adoptar.ui.user.fragments

import android.content.Context
import android.content.SharedPreferences
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.adoptar.adoptar.R
import com.adoptar.adoptar.modules.posts.domain.Post
import com.adoptar.adoptar.modules.posts.infrastructure.PostService
import com.adoptar.adoptar.ui.posts.viewModels.PostsViewModel
import com.adoptar.adoptar.ui.user.adapter.InterestedAdapter
import com.adoptar.adoptar.utils.TimeAgo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.time.LocalDateTime
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter

class InterestedFragment : Fragment() {

    lateinit var posts: List<Post>
    lateinit var v: View

    companion object {
        fun newInstance() = InterestedFragment()
    }

    private lateinit var viewModel: PostsViewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val prefs: SharedPreferences? =
            activity?.getSharedPreferences(getString(R.string.prefs_file), Context.MODE_PRIVATE)
        val userId: String? = prefs?.getString("id", null)
        activity?.runOnUiThread(java.lang.Runnable {
            GlobalScope.launch(Dispatchers.Main) {
                try {
                    posts = PostService.getPostsInterestedIn(userId!!)!!
                    val rvinterested = v.findViewById<View>(R.id.rv_published) as RecyclerView
                    val adapter = InterestedAdapter(posts) { position -> onItemClick(position) }
                    rvinterested.adapter = adapter
                    rvinterested.layoutManager = LinearLayoutManager(context)
                    calculateTimeAgo()
                }catch(error: Exception){
                    Toast.makeText(v.context, "Error de conexión con el servidor. Intente mas tarde.", Toast.LENGTH_SHORT).show()
                }
            }
        })
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        v = inflater.inflate(R.layout.interested_fragment, container, false)
        return v
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(PostsViewModel::class.java)
        // TODO: Use the ViewModel
    }

    fun onItemClick(position : Int){

        val action = UserFragmentDirections.actionNavigationProfileToPostDetailFragment(posts[position], false)
        v.findNavController().navigate(action)
    }


    fun calculateTimeAgo(){
        posts.forEach {
            var dateOk = it.createdAt?.substringBefore(".")
            var dateParsed = LocalDateTime.parse(dateOk, DateTimeFormatter.ISO_LOCAL_DATE_TIME)
            var dateSeconds: Long = dateParsed.toEpochSecond(ZoneOffset.UTC)
            it.timeAgo = TimeAgo.getTimeAgo(dateSeconds)
        }
    }
}