package com.adoptar.adoptar.ui.notifications.viewModels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.adoptar.adoptar.modules.notifications.domain.Notification
import com.adoptar.adoptar.modules.notifications.domain.NotificationIsRead
import com.adoptar.adoptar.modules.notifications.infraestructure.NotificationService
import com.adoptar.adoptar.modules.posts.domain.Post
import com.adoptar.adoptar.modules.posts.infrastructure.PostService
import kotlinx.coroutines.launch

class NotificationsViewModel : ViewModel() {

    val notifications = MutableLiveData<MutableList<Notification>>()
    var isLoading = MutableLiveData<Boolean>().apply { postValue(true)}
    var showRv = MutableLiveData<Boolean>().apply { postValue(true)}


    fun getNotifications(userId: String) {
        isLoading.value = true
        viewModelScope.launch {
            var notificationRet = NotificationService.getNotificationsById(userId)!!;
            notifications.value = notificationRet
            if(notifications.value!!.isEmpty()){
                showRv.value = false
            }
            isLoading.value = false
        }
    }

    fun updateNotificationsById(isRead: NotificationIsRead, notId: String){
        viewModelScope.launch {
            NotificationService.updateNotificationsById(isRead, notId)
        }
    }

    fun deleteNotificationsById(position: Int, userId: String) {
        viewModelScope.launch {
            NotificationService.deleteNotificationsById(notifications.value!!.get(position).id!!)
            getNotifications(userId)

        }
    }
}