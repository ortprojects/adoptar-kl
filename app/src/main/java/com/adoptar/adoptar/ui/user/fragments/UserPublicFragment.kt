package com.adoptar.adoptar.ui.user.fragments

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import androidx.viewpager2.widget.ViewPager2
import com.adoptar.adoptar.R
import com.adoptar.adoptar.modules.users.domain.User
import com.adoptar.adoptar.ui.user.adapter.TabAdapter
import com.adoptar.adoptar.ui.user.viewModels.UserViewModel
import com.google.android.material.tabs.TabLayoutMediator
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.fragment_profile.*

class UserPublicFragment : Fragment() {

    private lateinit var userViewModel: UserViewModel
    private lateinit var root: View
    private lateinit var btnEditProfile: ImageButton
    private lateinit var signOutButton: ImageButton
    private lateinit var viewPager: ViewPager2
    private lateinit var tabAdapter: TabAdapter
    private lateinit var textUserName: TextView
    private lateinit var User: User
    private lateinit var prefs: SharedPreferences
    private lateinit var displayName: String
    private lateinit var email: String
    private lateinit var id: String

    //private var isDetailDirections: Boolean  = false

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
//        User = UserFragmentArgs.fromBundle(requireArguments()).User;
        prefs = activity?.getSharedPreferences(
            getString(R.string.prefs_file),
            Context.MODE_PRIVATE
        )!!
        displayName = prefs?.getString("displayName", null).toString()
        email = prefs.getString("email", null).toString()
        id = prefs?.getString("id", null).toString()
        User = UserPublicFragmentArgs.fromBundle(requireArguments()).User!!

        userViewModel =
            ViewModelProviders.of(this).get(UserViewModel::class.java)
        root = inflater.inflate(R.layout.fragment_profile, container, false)
        btnEditProfile = root.findViewById(R.id.btnEditProfile)
        signOutButton = root.findViewById(R.id.btnSignOut)
        textUserName = root.findViewById(R.id.textUserName)
        btnEditProfile.setOnClickListener {
            val action =
                UserFragmentDirections.actionNavigationProfileToUserUpdateFragment()
            root.findNavController().navigate(action)
        }

        if (User.id.equals(id)) {
            btnEditProfile.visibility = View.VISIBLE
            signOutButton.visibility = View.VISIBLE
        } else {
            btnEditProfile.visibility = View.INVISIBLE
            signOutButton.visibility = View.INVISIBLE
        }

        signOutButton.setOnClickListener {
            //borrando datos
            val prefs: SharedPreferences.Editor? =
                activity?.getSharedPreferences(
                    getString(R.string.prefs_file),
                    Context.MODE_PRIVATE
                )
                    ?.edit()
            if (prefs != null) {
                prefs.clear()
                prefs.apply()
            }

            FirebaseAuth.getInstance().signOut()
            activity?.finishAffinity()
        }
        textUserName.text = User.displayName
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        tabAdapter = TabAdapter(this,true, User.id.toString())
        viewPager = pager
        viewPager.adapter = tabAdapter

        TabLayoutMediator(tab_layout, viewPager) { tab, position ->
            when (position) {
                0 -> {
                    tab.text = "Publicados"
                }
                1 -> {
                    tab.text = "Adoptados"
                }
                2 -> {
                    tab.text = "Reseñas"
                }
               /* 3 -> {
                    tab.text = "Interesados"
                }*/
            }
        }.attach()
    }

}