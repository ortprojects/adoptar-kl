package com.adoptar.adoptar.ui.notifications.fragments

import android.animation.Animator
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.TextureView
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.adoptar.adoptar.MainActivity
import com.adoptar.adoptar.R
import com.adoptar.adoptar.modules.notifications.domain.Notification
import com.adoptar.adoptar.modules.notifications.domain.NotificationIsRead
import com.adoptar.adoptar.modules.notifications.infraestructure.NotificationService
import com.adoptar.adoptar.ui.notifications.adapter.NotificationsAdapter
import com.adoptar.adoptar.ui.notifications.viewModels.NotificationsViewModel
import com.adoptar.adoptar.ui.posts.viewModels.PostsViewModel
import com.airbnb.lottie.LottieAnimationView
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class NotificationsFragment : Fragment() {

    lateinit var notifications: MutableList<Notification>
    lateinit var v: View
    lateinit var adapter: NotificationsAdapter
    lateinit var prgBar: LottieAnimationView
    private lateinit var navView: BottomNavigationView
    var userId: String = ""
    private val notificationsViewModel: NotificationsViewModel by activityViewModels()


    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        v = inflater.inflate(R.layout.fragment_notifications, container, false)
        session()


        prgBar = v.findViewById(R.id.prgBar)
        prgBar.setAnimation(R.raw.loadcat)
        prgBar.addAnimatorListener(object : Animator.AnimatorListener {
            override fun onAnimationStart(animation: Animator) {
            }

            override fun onAnimationEnd(animation: Animator) {
                prgBar.playAnimation()
            }

            override fun onAnimationCancel(animation: Animator) {
            }

            override fun onAnimationRepeat(animation: Animator) {
            }
        })

        notificationsViewModel.getNotifications(userId)
        navView = (activity as MainActivity?)!!.findViewById(R.id.nav_view)
        return v
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val rvNotifications = v.findViewById<View>(R.id.rv_notifications) as RecyclerView


        notificationsViewModel!!.showRv.observe(viewLifecycleOwner, Observer {
            if(it){
                rvNotifications.visibility = View.VISIBLE
            }else{
                v.findViewById<TextView>(R.id.noNotifications).visibility = View.VISIBLE
                rvNotifications.visibility = View.INVISIBLE
            }
        })

        notificationsViewModel!!.isLoading.observe(viewLifecycleOwner, Observer {
            if (it) {
                rvNotifications.visibility = View.INVISIBLE
                prgBar.visibility = View.VISIBLE
                v.findViewById<TextView>(R.id.noNotifications).visibility = View.INVISIBLE
                prgBar.playAnimation()
            } else {
                prgBar.visibility = View.GONE
                rvNotifications.visibility = View.VISIBLE
            }
        })

        notificationsViewModel!!.notifications.observe(viewLifecycleOwner, Observer {
            this.notifications = it
            if (this.notifications.isNotEmpty()) {
                var isRead = NotificationIsRead(true)
                for (notification in notifications) {
                    if (!notification.isRead!!) {
                        notificationsViewModel!!.updateNotificationsById(isRead, notification.id!!)
                    }
                }
            }
            removeBadge()
            adapter = NotificationsAdapter(notifications) { position -> onItemClick(position) }
            rvNotifications.adapter = adapter
            rvNotifications.layoutManager = LinearLayoutManager(context)
        })
    }

    fun onItemClick(position: Int) {
        notificationsViewModel.deleteNotificationsById(position, userId)
    }

    private fun removeBadge() {
        navView.getOrCreateBadge(R.id.navigation_notifications).isVisible = false
    }

    private fun session() {
        val prefs: SharedPreferences? =
                activity?.getSharedPreferences(getString(R.string.prefs_file), Context.MODE_PRIVATE)
        val id: String? = prefs?.getString("id", null)
        if (id != null) {
            userId = id
        }
    }

}