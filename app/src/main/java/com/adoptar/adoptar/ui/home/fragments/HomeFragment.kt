package com.adoptar.adoptar.ui.home.fragments

import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModelProviders
import com.adoptar.adoptar.R
import com.adoptar.adoptar.ui.home.viewModels.HomeViewModel
import com.adoptar.adoptar.ui.posts.fragments.PostDetailFragmentArgs
import com.adoptar.adoptar.ui.posts.viewModels.PostsViewModel


class HomeFragment : Fragment() {

    private lateinit var homeViewModel: HomeViewModel
    var currentLocation: Location? = null
    private lateinit var root : View
    private val postViewModel: PostsViewModel by activityViewModels()


   override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if(ActivityCompat.checkSelfPermission(root.context, android.Manifest.permission.ACCESS_COARSE_LOCATION)!= PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(root.context, android.Manifest.permission.ACCESS_FINE_LOCATION)!= PackageManager.PERMISSION_GRANTED) {

            requestPermissions( arrayOf( android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION ), 200)

            }else {
            var locationManager =
                requireActivity().getSystemService(Context.LOCATION_SERVICE) as LocationManager


            currentLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER)
            if (!HomeFragmentArgs.fromBundle(requireArguments()).cameFromFilters && currentLocation != null){
                postViewModel.getPosts(currentLocation!!.latitude.toString(), currentLocation!!.longitude.toString(), "5")
            }
            postViewModel.setIsFirstTime(false)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        homeViewModel =
            ViewModelProviders.of(this).get(HomeViewModel::class.java)
         root = inflater.inflate(R.layout.fragment_home, container, false)


        return root
    }

}