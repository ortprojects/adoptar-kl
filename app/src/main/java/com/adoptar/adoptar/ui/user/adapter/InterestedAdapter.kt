package com.adoptar.adoptar.ui.user.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import androidx.recyclerview.widget.RecyclerView
import com.adoptar.adoptar.R
import com.adoptar.adoptar.ui.posts.models.Post
import com.bumptech.glide.Glide
import com.google.android.material.card.MaterialCardView

class InterestedAdapter(private val mPosts: List<com.adoptar.adoptar.modules.posts.domain.Post>, val onItemClick: (Int) -> Unit) : RecyclerView.Adapter<InterestedAdapter.ViewHolder>()
    {
        // Provide a direct reference to each of the views within a data item
        // Used to cache the views within the item layout for fast access
        inner class ViewHolder(listItemView: View) : RecyclerView.ViewHolder(listItemView) {
            // Your holder should contain and initialize a member variable
            // for any view that will be set as you render a row
            val timeAgoTextView = itemView.findViewById<TextView>(R.id.card_post_timeAgo)
            val titleTextView = itemView.findViewById<TextView>(R.id.card_post_title)
            val nameTextView = itemView.findViewById<TextView>(R.id.card_post_name)
            val genderTextView = itemView.findViewById<TextView>(R.id.card_post_gender)
            val ageTextView = itemView.findViewById<TextView>(R.id.card_post_age)
            val image = itemView.findViewById<AppCompatImageView>(R.id.card_image)

            fun getCardLayout (): MaterialCardView {
                return itemView.findViewById(R.id.card_package_item)
            }
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val context = parent.context
            val inflater = LayoutInflater.from(context)
            // Inflate the custom layout
            val postView = inflater.inflate(R.layout.post_fragment, parent, false)
            // Return a new holder instance
            return ViewHolder(postView)
        }

        // Involves populating data into the item through holder
        override fun onBindViewHolder(viewHolder: InterestedAdapter.ViewHolder, position: Int) {
            // Get the data model based on position
            val post: com.adoptar.adoptar.modules.posts.domain.Post = mPosts[position]
            // Set item views based on your views and data model
            val timeAgoView = viewHolder.timeAgoTextView
            timeAgoView.text = post.timeAgo
            val titleView = viewHolder.titleTextView
            titleView.text = post.title
            val nameView = viewHolder.nameTextView
            nameView.text = post.petName
            val genderView = viewHolder.genderTextView
            genderView.text = post.petSex
            val ageView = viewHolder.ageTextView
            ageView.text = post.petAge
            val media = post.imageFiles!![0]
            if (media !== null) {
                Glide.with(viewHolder.image.context)
                    .load(media)
                    .into(viewHolder.image)
            } else {
                viewHolder.image.setImageResource(R.drawable.imagotipo)
            }
            val card = viewHolder.getCardLayout()
            card.setOnClickListener(){
                onItemClick(position)
            }
        }

        // Returns the total count of items in the list
        override fun getItemCount(): Int {
            return mPosts.size
        }
}