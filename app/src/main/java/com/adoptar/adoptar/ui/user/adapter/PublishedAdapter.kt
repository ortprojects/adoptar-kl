package com.adoptar.adoptar.ui.user.adapter

import android.content.Context
import android.content.SharedPreferences
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import androidx.recyclerview.widget.RecyclerView
import com.adoptar.adoptar.R
import com.adoptar.adoptar.modules.posts.domain.Post
import com.bumptech.glide.Glide
import com.google.android.material.card.MaterialCardView

class PublishedAdapter(private val mPosts: List<Post>, val idUser: String, val context : Context,val onItemClick: (Int, Any?) -> Unit) :
    RecyclerView.Adapter<PublishedAdapter.ViewHolder>() {
    inner class ViewHolder(listItemView: View) : RecyclerView.ViewHolder(listItemView) {
        val timeAgoTextView = itemView.findViewById<TextView>(R.id.card_post_timeAgo)
        val titleTextView = itemView.findViewById<TextView>(R.id.card_post_title)
        val nameTextView = itemView.findViewById<TextView>(R.id.card_post_name)
        val genderTextView = itemView.findViewById<TextView>(R.id.card_post_gender)
        val ageTextView = itemView.findViewById<TextView>(R.id.card_post_age)
        val image = itemView.findViewById<AppCompatImageView>(R.id.card_image)
        val btnEye = itemView.findViewById<ImageButton>(R.id.btnEye)
        val imgPaw = itemView.findViewById<ImageView>(R.id.imgPaw)

        fun getCardLayout(): MaterialCardView {
            return itemView.findViewById(R.id.card_package_item)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)
        val postView = inflater.inflate(R.layout.post_fragment, parent, false)
        return ViewHolder(postView)
    }

    override fun onBindViewHolder(viewHolder: PublishedAdapter.ViewHolder, position: Int) {

        val prefs: SharedPreferences? =
            context.getSharedPreferences(context.getString(R.string.prefs_file), Context.MODE_PRIVATE)
        var idLogin = prefs?.getString("id", null).toString()

        viewHolder.btnEye.visibility = View.INVISIBLE
        val post: Post = mPosts[position]
        if(post.adoptedBy?.id != null){
            viewHolder.imgPaw.visibility = View.VISIBLE
        }
        if(idUser == idLogin){
            viewHolder.btnEye.visibility = View.VISIBLE
        }
        val timeAgoView = viewHolder.timeAgoTextView
        timeAgoView.text = post.timeAgo
        val titleView = viewHolder.titleTextView
        titleView.text = post.title
        val nameView = viewHolder.nameTextView
        nameView.text = post.petName
        val genderView = viewHolder.genderTextView
        genderView.text = post.petSex
        val ageView = viewHolder.ageTextView
        ageView.text = post.petAge
        val media = post.imageFiles!!.get(0)
        if (media !== null) {
            Glide.with(viewHolder.image.context)
                .load(media)
                .into(viewHolder.image)
        } else {
            viewHolder.image.setImageResource(R.drawable.imagotipo)
        }
        val card = viewHolder.getCardLayout()
        card.setOnClickListener() {
            onItemClick(position, false)
        }

        val btnEye = viewHolder.btnEye

        btnEye.setOnClickListener() {
            onItemClick(position, true)
        }
    }

    override fun getItemCount(): Int {
        return mPosts.size
    }
}

