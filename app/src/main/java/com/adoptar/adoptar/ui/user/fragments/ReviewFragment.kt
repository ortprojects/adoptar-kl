package com.adoptar.adoptar.ui.user.fragments

import android.content.Context
import android.content.SharedPreferences
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.adoptar.adoptar.R
import com.adoptar.adoptar.modules.messages.domain.Review
import com.adoptar.adoptar.modules.messages.infrastructure.ReviewServices
import com.adoptar.adoptar.ui.user.adapter.ReviewsAdapter
import com.adoptar.adoptar.ui.user.viewModels.ReviewViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class ReviewFragment(id: String?) : Fragment() {

    var idUser = id

    lateinit var reviews: MutableList<Review>
    lateinit var v: View
    lateinit var idActiveUser: String
    private lateinit var viewModel: ReviewViewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val prefs: SharedPreferences? =  activity?.getSharedPreferences(getString(R.string.prefs_file), Context.MODE_PRIVATE)

        activity?.runOnUiThread(java.lang.Runnable {
            GlobalScope.launch(Dispatchers.Main) {
                try{
                    idActiveUser = prefs?.getString("id", null).toString()
                    reviews = ReviewServices.getReviewsByUserId(idUser!!)!!

                    val rvReviews = v.findViewById<View>(R.id.rv_reviews) as RecyclerView
                    // Create adapter passing in the sample user data
                    val adapter = ReviewsAdapter(reviews, context, v, idActiveUser)
                    // Attach the adapter to the recyclerview to populate items
                    rvReviews.adapter = adapter
                    // Set layout manager to position the items
                    rvReviews.layoutManager = LinearLayoutManager(context)
                }catch(error: Exception){
                    Log.i("error: ", error.message)
                    Toast.makeText(v.context, "Error de conexión con el servidor. Intente mas tarde.", Toast.LENGTH_SHORT).show()
                }
            }
        })
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        v = inflater.inflate(R.layout.review_fragment, container, false)

        return v
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(ReviewViewModel::class.java)
        // TODO: Use the ViewModel
    }

}