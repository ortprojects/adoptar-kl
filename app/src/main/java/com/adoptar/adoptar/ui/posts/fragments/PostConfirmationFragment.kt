package com.adoptar.adoptar.ui.posts.fragments

import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.widget.AppCompatImageView
import androidx.core.content.ContextCompat
import androidx.navigation.findNavController
import com.adoptar.adoptar.R
import com.adoptar.adoptar.modules.posts.domain.AdoptionConfirm
import com.adoptar.adoptar.modules.posts.domain.FormAdoption
import com.adoptar.adoptar.modules.posts.domain.Post
import com.adoptar.adoptar.modules.posts.infrastructure.PostService
import com.adoptar.adoptar.modules.users.domain.User
import com.bumptech.glide.Glide
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.File

class PostConfirmationFragment : Fragment() {

    lateinit var v : View

    lateinit var adoptantUser : User
    lateinit var post : Post
    //lateinit var form : Form

    lateinit var textConfUserName : TextView
    lateinit var textConfMessage : TextView
    lateinit var imgConfUser : ImageView
    lateinit var formAdoption: FormAdoption
    lateinit var petImg : ImageView
    lateinit var aboutPeople : TextView
    lateinit var aboutPets : TextView
    lateinit var aboutPetCastrated : TextView
    lateinit var hasNoPet : TextView
    lateinit var locationType : TextView
    lateinit var btnReject : Button
    lateinit var btnConfirm : Button


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        GlobalScope.launch(Dispatchers.Main) {
            formAdoption = PostService.getFormAdoption(adoptantUser.id!!, post.id!!)
            aboutPeople.text = formAdoption.aboutPeople
            aboutPets.text = formAdoption.aboutPets
            aboutPetCastrated.text = formAdoption.aboutPetCastrated
            hasNoPet.text = formAdoption.hasNoPet
            locationType.text = formAdoption.locationType
        }

        btnReject.setOnClickListener {
            try{
                GlobalScope.launch(Dispatchers.Main) {
                    PostService.deleteInterested(adoptantUser.id!!, post.id!!)
                    Toast.makeText(v.context, "Postulante eliminado exitosamente.",Toast.LENGTH_LONG).show()
                    val action = PostConfirmationFragmentDirections.actionPostConfirmationFragmentToInterestedOnFragment(post)
                    v.findNavController().navigate(action)
                }
            }catch(error: Exception){
                Toast.makeText(v.context, "Error de conexión con el servidor. Intente mas tarde.",Toast.LENGTH_LONG).show()
            }
        }

        btnConfirm.setOnClickListener{
            val adoptionConfirm = AdoptionConfirm(post.id!!, adoptantUser.id!!)
            try{
                GlobalScope.launch(Dispatchers.Main){
                    PostService.confirmAdoption(adoptionConfirm)
                    Toast.makeText(v.context, "Postulante confirmado exitosamente.",Toast.LENGTH_LONG).show()
                    val action = PostConfirmationFragmentDirections.actionPostConfirmationFragmentToNavigationProfile()
                    v.findNavController().navigate(action)
                }
            }catch(error: Exception){
                Toast.makeText(v.context, "Error de conexión con el servidor. Intente mas tarde.",Toast.LENGTH_LONG).show()
            }
        }

        textConfUserName.text = adoptantUser.displayName
        textConfMessage.text = "¡Quiere adoptar a " + post.petName + "!"
        imgConfUser.setImageURI(Uri.parse(adoptantUser.photoURL))
        val media = post.imageFiles?.get(0)
        if (media !== null) {
            Glide.with(petImg.context)
                .load(media)
                .into(petImg)
        } else {
            petImg.setImageResource(R.drawable.imagotipo)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        v = inflater.inflate(R.layout.fragment_post_confirmation, container, false)

        post = PostConfirmationFragmentArgs.fromBundle(requireArguments()).Post
        adoptantUser = PostConfirmationFragmentArgs.fromBundle(requireArguments()).User

        textConfUserName = v.findViewById(R.id.textConfUserName)
        textConfMessage = v.findViewById(R.id.textConfMessage)
        imgConfUser = v.findViewById(R.id.imgConfUser)

        petImg = v.findViewById(R.id.petImg)
        aboutPeople = v.findViewById(R.id.aboutPeople)
        aboutPets = v.findViewById(R.id.aboutPets)
        aboutPetCastrated = v.findViewById(R.id.aboutPetCastrated)
        hasNoPet = v.findViewById(R.id.hasNoPet)
        locationType = v.findViewById(R.id.locationType)

        btnReject = v.findViewById(R.id.btnReject)
        btnReject.setBackgroundColor(ContextCompat.getColor(v.context, R.color.adp_primary))
        btnConfirm = v.findViewById(R.id.btnConfirm)
        btnConfirm.setBackgroundColor(ContextCompat.getColor(v.context, R.color.adp_primary))

        return v
    }

    companion object {
        fun newInstance() = PostConfirmationFragment()
    }

}