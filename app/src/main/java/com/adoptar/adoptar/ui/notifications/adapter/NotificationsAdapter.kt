package com.adoptar.adoptar.ui.notifications.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.adoptar.adoptar.R
import com.adoptar.adoptar.modules.notifications.domain.Notification

class NotificationsAdapter(private val mNotifications: MutableList<Notification>, val onItemClick: (Int) -> Unit) :
    RecyclerView.Adapter<NotificationsAdapter.ViewHolder>() {

    var items = mNotifications
    inner class ViewHolder(listItemView: View) : RecyclerView.ViewHolder(listItemView) {
        val notificationMessage: TextView = itemView.findViewById<TextView>(R.id.notificationMessage)
        val notificationDate: TextView = itemView.findViewById<TextView>(R.id.notificationDate)
        val deleteNotification: ImageButton = itemView.findViewById<ImageButton>(R.id.deleteNotification)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotificationsAdapter.ViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)
        val notificationView = inflater.inflate(R.layout.fragment_notification, parent, false)
        return ViewHolder(notificationView)
    }

    override fun onBindViewHolder(viewHolder: NotificationsAdapter.ViewHolder, position: Int) {
        val notification: Notification = items[position]
        val notificationMessage = viewHolder.notificationMessage
        notificationMessage.text = notification.message
        val notificationDate = viewHolder.notificationDate
        notificationDate.text = notification.createdAt?.split("T")?.get(0)

        val deleteNotification = viewHolder.deleteNotification

        deleteNotification.setOnClickListener() {
            onItemClick(position)
        }
    }

    override fun getItemCount(): Int {
        return mNotifications.size
    }
}