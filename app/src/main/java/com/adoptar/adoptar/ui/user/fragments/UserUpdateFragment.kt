package com.adoptar.adoptar.ui.user.fragments

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.location.Geocoder
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.adoptar.adoptar.MainActivity
import com.adoptar.adoptar.R
import com.adoptar.adoptar.modules.users.domain.Address
import com.adoptar.adoptar.modules.users.infrastructure.UserServices
import com.google.android.gms.common.api.Status
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.api.net.PlacesClient
import com.google.android.libraries.places.widget.AutocompleteSupportFragment
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.*

class UserUpdateFragment : Fragment() {

    // inizitalice user with direction args
    //lateinit var user: UserFragmentArgs
    //var user = User("Gonza", "Garcia", "1568485222", "38456821", "Calle falsa 123", "gonzalo.garcia@gmail.com", "asd123asd")

    lateinit var v: View
    lateinit var placesClient: PlacesClient
    lateinit var btnSend: Button
    lateinit var txtFirstName: EditText
    lateinit var txtPhone: EditText
    lateinit var txtDNI: EditText

    var address: Place? = null
    lateinit var user: com.adoptar.adoptar.modules.users.domain.User
    var addresString: String? = null


    var placeFields = Arrays.asList(
        Place.Field.ID,
        Place.Field.NAME,
        Place.Field.ADDRESS,
        Place.Field.LAT_LNG
    )

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.runOnUiThread(java.lang.Runnable {
            GlobalScope.launch(Dispatchers.Main) {
                val geocode = Geocoder(v.context)
                val prefs: SharedPreferences? =
                    activity?.getSharedPreferences(getString(R.string.prefs_file), Context.MODE_PRIVATE)
                val id: String? = prefs?.getString("id", null)
                user = id?.let { UserServices.userById(it) }!!
                Log.d("LATITUD", user.address?.latitude.toString() +" - " + user.address?.longitude.toString())
                var addressFull =
                    geocode.getFromLocation(user.address?.latitude!!, user.address?.longitude!!, 1)

                Log.d("Direccionnnnnnnn", addressFull.toString())


                addresString = addressFull[0].getAddressLine(0)

                txtFirstName = v.findViewById(R.id.txtFirstName)
                txtPhone = v.findViewById(R.id.txtPhone)
                txtDNI = v.findViewById(R.id.txtDNI)
                val autocompleteFragment = childFragmentManager
                    .findFragmentById(R.id.autocomplete_fragment) as AutocompleteSupportFragment
                autocompleteFragment.setHint(addresString)
                txtFirstName.setText(user.displayName)
                txtPhone.setText(user.phoneNumber)
                txtDNI.setText(user.dni)

            }
        })
        btnSend = v.findViewById(R.id.btnSend)
        btnSend.setOnClickListener {
            completeForm()
        }
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
         v = inflater.inflate(R.layout.user_update_fragment, container, false)



        return v
    }

    override fun onStart() {
        super.onStart()
        initPlaces()
        setupPlacesAutocomplete()
    }

    private fun setupPlacesAutocomplete() {
        val autocompleteFragment = childFragmentManager
            .findFragmentById(R.id.autocomplete_fragment) as AutocompleteSupportFragment
        autocompleteFragment.setPlaceFields(placeFields)

        autocompleteFragment.setHint(addresString)
        val etTextInput: EditText? = autocompleteFragment.view?.findViewById(R.id.places_autocomplete_search_input)
        if (etTextInput != null) {
            etTextInput.setTextColor(Color.WHITE)
        }

        autocompleteFragment.setOnPlaceSelectedListener(object : PlaceSelectionListener {
            override fun onPlaceSelected(p0: Place) {
                address = p0
            }

            override fun onError(p0: Status) {
            }
        })
        autocompleteFragment.view?.findViewById<View>(R.id.places_autocomplete_clear_button)
            ?.setOnClickListener() {
                address = null
                autocompleteFragment.setText("")
            }
    }

    private fun initPlaces() {
        Places.initialize(v.context, "AIzaSyCFg3S_ycV418XGqN1FctIsT5S02XXqdcE")
        placesClient = Places.createClient(v.context)
    }

    private fun completeForm() {
        if (validatedFields()) {
            if(address != null){
                var addressUser = address?.latLng?.latitude?.let {
                    address?.latLng?.longitude?.let { it1 ->
                        Address("",
                            it, it1
                        )
                    }
                }
                user.address = addressUser
            }
            user.displayName = txtFirstName.text.toString()
            user.dni = txtDNI.text.toString()
            user.phoneNumber = txtPhone.text.toString()

            GlobalScope.launch(Dispatchers.IO) {
                val msgResponse = UserServices.userPut(user)
                withContext(Dispatchers.Main) {
                    Toast.makeText(v.context, msgResponse, Toast.LENGTH_LONG).show()
                    val mainIntent: Intent = Intent(v.context, MainActivity::class.java).apply() {
                        putExtra("email", user.email)
                        putExtra("id", user.id)
                        putExtra("displayName", user.displayName)
                    }
                    startActivity(mainIntent)
                }
            }

        } else {
            Toast.makeText(v.context, "Todos los campos deben ser completados", Toast.LENGTH_SHORT)
                .show()
        }
    }

    private fun validatedFields(): Boolean {
        return !(txtFirstName.text.isEmpty() ||
                txtPhone.text.isEmpty() ||
                txtDNI.text.isEmpty())
    }
}