package com.adoptar.adoptar.ui.posts.fragments

import android.content.Context
import android.content.Context.INPUT_METHOD_SERVICE
import android.content.SharedPreferences
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.adoptar.adoptar.R
import com.adoptar.adoptar.modules.messages.domain.Question
import com.adoptar.adoptar.modules.messages.domain.QuestionPost
import com.adoptar.adoptar.modules.messages.infrastructure.QuestionServices
import com.adoptar.adoptar.modules.messages.infrastructure.QuestionServices.Companion.questionPost
import com.adoptar.adoptar.ui.posts.adapter.QuestionsAdapter
import com.adoptar.adoptar.modules.posts.domain.Post
import com.adoptar.adoptar.ui.posts.viewModels.PostQuestionsViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class PostQuestionsFragment : Fragment() {

    lateinit var questions: MutableList<Question>
    lateinit var v: View
    lateinit var post: Post
    lateinit var idPost: String
    lateinit var idUserPost: String
    lateinit var idActiveUser: String
    lateinit var textSinPreguntas: TextView
    lateinit var btnNewQuestion: Button
    private lateinit var viewModel: PostQuestionsViewModel

    companion object {
        fun newInstance() = PostQuestionsFragment()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        post = PostQuestionsFragmentArgs.fromBundle(requireArguments()).post
        idUserPost = post.createdBy?.id.toString()
        idPost = post.id.toString()
        val prefs: SharedPreferences? =  activity?.getSharedPreferences(getString(R.string.prefs_file), Context.MODE_PRIVATE)
        idActiveUser = prefs?.getString("id", null).toString()
        textSinPreguntas = v.findViewById<TextView>(R.id.textSinPreguntas)
        btnNewQuestion = v.findViewById<Button>(R.id.btnNewQuestion)

        activity?.runOnUiThread(java.lang.Runnable {
            GlobalScope.launch(Dispatchers.Main) {
                try {
                    viewLoad()
                }catch(error: Exception){
                    Log.i("error: ", error.message)
                   Toast.makeText(v.context, "Error de conexión con el servidor. Intente mas tarde.", Toast.LENGTH_SHORT).show()
                }
            }

            btnNewQuestion.setOnClickListener{
                GlobalScope.launch(Dispatchers.Main) {
                    try{
                        onItemClick()
                        viewLoad()

                    }catch(error: Exception){
                        Toast.makeText(v.context, "Error de conexión con el servidor. Intente mas tarde.", Toast.LENGTH_SHORT).show()
                    }
                }
            }
        })
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        v = inflater.inflate(R.layout.post_questions_fragment, container, false)
        return v
    }

    suspend fun onItemClick(){
        val pregunta =  v.findViewById<EditText>(R.id.editTextQuestion)
        if ( pregunta.text.toString() != "") {
            questionPost(QuestionPost(idActiveUser, pregunta.text.toString()), idPost)
            pregunta.text.clear()
            hideKeyboard()
            Toast.makeText(v.context, "Pregunta creada exitosamente.", Toast.LENGTH_SHORT).show()
        } else {
            hideKeyboard()
            Toast.makeText(v.context, "No pueden ingresarse preguntas vacías !!!", Toast.LENGTH_SHORT).show()
        }
    }

    private fun hideKeyboard(){
        val imm = context?.getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(v.windowToken, 0)
    }

    suspend fun viewLoad(){
        val editTextQuestion = v.findViewById<EditText>(R.id.editTextQuestion)
        var owner = false

        questions = QuestionServices.getQuestionByPost(idPost)!!

        if((idActiveUser == idUserPost) || (post.adoptedBy?.id != null)){
            owner = true
            editTextQuestion.visibility = View.GONE
            btnNewQuestion.visibility = View.GONE
        }

        if (questions.size != 0){
            textSinPreguntas.visibility = View.GONE
            questions.sortBy { question -> question.createdAt }
            questions.reverse()
        }



        val rvQuestions = v.findViewById<View>(R.id.rv_questions) as RecyclerView
        val adapter = QuestionsAdapter(questions, idPost, context, v, owner, idActiveUser)

        rvQuestions.adapter = adapter
        rvQuestions.layoutManager = LinearLayoutManager(context)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(PostQuestionsViewModel::class.java)
        // TODO: Use the ViewModel
    }

}