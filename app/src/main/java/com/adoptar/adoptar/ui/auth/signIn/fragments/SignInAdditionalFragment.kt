package com.adoptar.adoptar.ui.auth.signIn.fragments

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.adoptar.adoptar.MainActivity
import com.adoptar.adoptar.R
import com.adoptar.adoptar.modules.users.domain.Address
import com.adoptar.adoptar.modules.users.domain.User
import com.adoptar.adoptar.modules.users.infrastructure.UserServices
import com.google.android.gms.common.api.Status
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.api.net.PlacesClient
import com.google.android.libraries.places.widget.AutocompleteSupportFragment
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.*


class SignInAdditionalFragment : Fragment() {
    lateinit var user : User
    lateinit var v: View
    lateinit var placesClient: PlacesClient
    lateinit var btnSend: Button
    lateinit var txtFirstName: EditText
    lateinit var txtLastName: EditText
    lateinit var txtPhone: EditText
    lateinit var txtDNI: EditText
    var address: Place? = null

    var placeFields = Arrays.asList(
        Place.Field.ID,
        Place.Field.NAME,
        Place.Field.ADDRESS,
        Place.Field.LAT_LNG
    )

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.sign_in_additional_fragment, container, false)

        txtFirstName = v.findViewById(R.id.txtFirstName)
        txtLastName = v.findViewById(R.id.txtLastName)
        txtPhone = v.findViewById(R.id.txtPhone)
        txtDNI = v.findViewById(R.id.txtDNI)

        btnSend = v.findViewById(R.id.btnSend)
        btnSend.setOnClickListener {
            completeForm()
        }

        return v
    }

    override fun onStart() {
        super.onStart()
        user = SignInAdditionalFragmentArgs.fromBundle(requireArguments()).User
        initPlaces()
        setupPlacesAutocomplete()
    }

    private fun setupPlacesAutocomplete() {
        val autocompleteFragment = childFragmentManager
            .findFragmentById(R.id.autocomplete_fragment) as AutocompleteSupportFragment
        autocompleteFragment.setPlaceFields(placeFields)

        autocompleteFragment.setHint("Buscar")
        val etTextInput: EditText? = autocompleteFragment.view?.findViewById(R.id.places_autocomplete_search_input)
        if (etTextInput != null) {
            etTextInput.setTextColor(Color.WHITE)
        }

        autocompleteFragment.setOnPlaceSelectedListener(object : PlaceSelectionListener {
            override fun onPlaceSelected(p0: Place) {
                address = p0
            }

            override fun onError(p0: Status) {
            }
        })
        autocompleteFragment.view?.findViewById<View>(R.id.places_autocomplete_clear_button)
            ?.setOnClickListener() {
                address = null
                autocompleteFragment.setText("")
            }
    }

    private fun initPlaces() {
        Places.initialize(v.context, "AIzaSyCFg3S_ycV418XGqN1FctIsT5S02XXqdcE")
        placesClient = Places.createClient(v.context)
    }

    private fun completeForm() {
        if (validatedFields()) {
            var addressUser = Address("",address!!.latLng!!.latitude!!, address!!.latLng!!.longitude!!)
            user.address = addressUser
            user.displayName = txtFirstName.text.toString() + " " + txtLastName.text.toString()
            user.dni = txtDNI.text.toString()
            user.phoneNumber = txtPhone.text.toString()
            user.isFirstTime = false
            Log.d("USUARIO POSTTTTTTTTTTTTT", user.toString())
            GlobalScope.launch(Dispatchers.IO) {
                val msgResponse = UserServices.userPut(user)
                withContext(Dispatchers.Main) {
                    Toast.makeText(v.context, msgResponse, Toast.LENGTH_LONG).show()
                    val mainIntent: Intent = Intent(v.context, MainActivity::class.java).apply() {
                        putExtra("email", user.email)
                        putExtra("id", user.id)
                        putExtra("displayName", user.displayName)
                    }
                    startActivity(mainIntent)
                }
            }

        } else {
            Toast.makeText(v.context, "Todos los campos deben ser completados", Toast.LENGTH_SHORT)
                .show()
        }
    }

    private fun validatedFields(): Boolean {
        return !(txtFirstName.text.isEmpty() ||
                txtLastName.text.isEmpty() ||
                txtPhone.text.isEmpty() ||
                txtDNI.text.isEmpty() || address == null)
    }
}