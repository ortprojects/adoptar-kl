package com.adoptar.adoptar.ui.user.adapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import androidx.recyclerview.widget.RecyclerView
import com.adoptar.adoptar.R
import com.adoptar.adoptar.modules.users.domain.User

import com.google.android.material.card.MaterialCardView

class UserAdapter(private val userList: List<User>, val onItemClick: (Int) -> Unit) :
    RecyclerView.Adapter<UserAdapter.ViewHolder>() {

    inner class ViewHolder(listItemView: View) : RecyclerView.ViewHolder(listItemView) {
        val imgProfile = itemView.findViewById<AppCompatImageView>(R.id.imgProfile)
        val txtName = itemView.findViewById<TextView>(R.id.txtName)

        fun getCardLayout (): MaterialCardView {
            return itemView.findViewById(R.id.cardInterestedItem)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)
        // Inflate the custom layout
        val interestedView = inflater.inflate(R.layout.interested_on_item_fragment, parent, false)
        // Return a new holder instance
        return ViewHolder(interestedView)
    }

    override fun onBindViewHolder(viewHolder: UserAdapter.ViewHolder, position: Int) {
        // Get the data model based on position
        val interested: User = userList[position]
        // Set item views based on your views and data model

        val nameView = viewHolder.txtName
        nameView.text = interested.displayName

        val imgProfile = viewHolder.imgProfile
        viewHolder.imgProfile.setImageResource(R.drawable.imagotipo)

        val card = viewHolder.getCardLayout()
        card.setOnClickListener(){
            onItemClick(position)
        }
    }

    override fun getItemCount(): Int {
        return userList.size
    }
}
