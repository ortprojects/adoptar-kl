package com.adoptar.adoptar.ui.user.fragments

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.adoptar.adoptar.R
import com.adoptar.adoptar.ui.user.viewModels.ItemReviewViewModel

class ItemReviewFragment : Fragment() {

    companion object {
        fun newInstance() = ItemReviewFragment()
    }

    private lateinit var viewModel: ItemReviewViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.item_review_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(ItemReviewViewModel::class.java)
        // TODO: Use the ViewModel
    }

}