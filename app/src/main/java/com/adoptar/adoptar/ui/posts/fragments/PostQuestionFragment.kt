package com.adoptar.adoptar.ui.posts.fragments

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.adoptar.adoptar.R
import com.adoptar.adoptar.ui.posts.viewModels.PostQuestionViewModel

class PostQuestionFragment : Fragment() {

    companion object {
        fun newInstance() = PostQuestionFragment()
    }

    private lateinit var viewModel: PostQuestionViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.post_question_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(PostQuestionViewModel::class.java)
        // TODO: Use the ViewModel
    }

}