package com.adoptar.adoptar.ui.posts.fragments

import android.animation.Animator
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.navigation.NavDirections
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.adoptar.adoptar.R
import com.adoptar.adoptar.modules.posts.domain.Post
import com.adoptar.adoptar.ui.home.fragments.HomeFragmentDirections
import com.adoptar.adoptar.ui.posts.adapter.PostsAdapter
import com.adoptar.adoptar.ui.posts.viewModels.PostsViewModel
import com.adoptar.adoptar.utils.TimeAgo
import java.time.LocalDateTime
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import com.airbnb.lottie.LottieAnimationView
import kotlinx.android.synthetic.main.activity_splash.*
import org.w3c.dom.Text


class PostsFragment : Fragment() {

    lateinit var posts: List<Post>
    lateinit var v: View
    lateinit var prgBar: LottieAnimationView
    lateinit var txtNotResult: TextView

    private val postViewModel: PostsViewModel by activityViewModels()

    companion object {
        fun newInstance() = PostsFragment()
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        v = inflater.inflate(R.layout.posts_fragment, container, false)


        prgBar = v.findViewById(R.id.prg_bar)
        prgBar.setAnimation(R.raw.loadcat)
        prgBar.addAnimatorListener(object : Animator.AnimatorListener {
            override fun onAnimationStart(animation: Animator) {
            }

            override fun onAnimationEnd(animation: Animator) {
                prgBar.playAnimation()
            }

            override fun onAnimationCancel(animation: Animator) {
            }

            override fun onAnimationRepeat(animation: Animator) {
            }
        })


        v.findViewById<ImageButton>(R.id.addPost).setOnClickListener {
            val action =
                    HomeFragmentDirections.actionNavigationHomeToNewPostFragment(createEmptyPost(),false)
            v.findNavController().navigate(action)
        }

        val fab = v.findViewById<View>(R.id.filter)
        fab.setOnClickListener(
            Navigation.createNavigateOnClickListener(
                R.id.action_navigation_home_to_postFilters,
                null
            )
        )

        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val rvPosts = v.findViewById<View>(R.id.rv_posts) as RecyclerView
        txtNotResult = v.findViewById(R.id.txtNotResult)
        postViewModel.isLoading.observe(viewLifecycleOwner, Observer {
            if (it) {
                rvPosts.visibility = View.GONE
                prgBar.visibility = View.VISIBLE
                prgBar.playAnimation()
            } else {
                prgBar.visibility = View.GONE
                rvPosts.visibility = View.VISIBLE
            }
        })
        try {
            postViewModel!!.posts.observe(viewLifecycleOwner, Observer {
                this.posts = it
                val adapter =
                        PostsAdapter(posts) { position, interested -> onItemClick(position, false) }
                rvPosts.adapter = adapter
                rvPosts.layoutManager = LinearLayoutManager(context)
            })
        } catch (error: Exception) {
            Toast.makeText(v.context, "Error de conexion con el servidor", Toast.LENGTH_LONG).show()
        }

        postViewModel!!.isEmptyResult.observe(viewLifecycleOwner, Observer {
            if (it) {
                txtNotResult.visibility = View.VISIBLE
            } else {
                txtNotResult.visibility = View.GONE
            }
        })
    }

    fun onItemClick(position: Int, interested: Boolean) {
        var action: NavDirections? = null
        if (interested)
            action =
                    HomeFragmentDirections.actionNavigationHomeToInterestedOnFragment(posts[position])
        else
            action =
                    HomeFragmentDirections.actionNavigationHomeToPostDetailFragment(
                            posts[position], false
                    )

        v.findNavController().navigate(action)
    }

    private fun calculateTimeAgo() {
        posts.forEach {
            var dateOk = it.createdAt?.substringBefore(".")
            var dateParsed = LocalDateTime.parse(dateOk, DateTimeFormatter.ISO_LOCAL_DATE_TIME)
            var dateSeconds: Long = dateParsed.toEpochSecond(ZoneOffset.UTC)
            it.timeAgo = TimeAgo.getTimeAgo(dateSeconds)
        }
    }

    fun onEyeClick(position: Int) {
        val action =
                HomeFragmentDirections.actionNavigationHomeToInterestedOnFragment(posts[position])
        v.findNavController().navigate(action)
    }

    private fun createEmptyPost(): Post {
        return Post(
                "",
                null,
                0.0,
                0.0,
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                null,
                null,
                false
        )
    }
}


