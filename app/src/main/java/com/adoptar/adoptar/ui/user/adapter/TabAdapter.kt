package com.adoptar.adoptar.ui.user.adapter

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.adoptar.adoptar.ui.user.fragments.AdoptedFragment
import com.adoptar.adoptar.ui.user.fragments.InterestedFragment
import com.adoptar.adoptar.ui.user.fragments.PublishedFragment
import com.adoptar.adoptar.ui.user.fragments.ReviewFragment

class TabAdapter(fragment: Fragment, isPublic: Boolean, id: String) :
    FragmentStateAdapter(fragment) {

    var id = id
    var isPublic = isPublic

    override fun getItemCount(): Int {

        if (isPublic) {
            return 3
        } else
            return 4
    }

    override fun createFragment(position: Int): Fragment {

        if (itemCount == 4) {
            return when (position) {
                0 -> {
                    PublishedFragment(id)
                }
                1 -> {
                    AdoptedFragment(id)
                }
                2 -> {
                    ReviewFragment(id)
                }
                3 -> {
                    InterestedFragment()
                }
                else -> PublishedFragment(id)
            }
        } else {
            return when (position) {
                0 -> {
                    PublishedFragment(id)
                }
                1 -> {
                    AdoptedFragment(id)
                }
                2 -> {
                    ReviewFragment(id)
                }
                else -> PublishedFragment(id)
            }
        }
    }
}