package com.adoptar.adoptar.ui.user.fragments

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.NavDirections
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.adoptar.adoptar.R
import com.adoptar.adoptar.modules.posts.domain.Post
import com.adoptar.adoptar.modules.posts.infrastructure.PostService
import com.adoptar.adoptar.modules.users.domain.User
import com.adoptar.adoptar.ui.home.fragments.HomeFragmentDirections
import com.adoptar.adoptar.ui.posts.adapter.PostsAdapter
import com.adoptar.adoptar.ui.posts.fragments.PostDetailFragmentArgs
import com.adoptar.adoptar.ui.posts.models.Pet
import com.adoptar.adoptar.ui.posts.viewModels.PostsViewModel
import com.adoptar.adoptar.ui.user.adapter.InterestedAdapter
import com.adoptar.adoptar.ui.user.adapter.UserAdapter
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

import java.util.ArrayList


class InterestedOnFragment : Fragment() {
    lateinit var v: View
    lateinit var post : Post
    lateinit var users: List<User>

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val prefs: SharedPreferences? =
            activity?.getSharedPreferences(getString(R.string.prefs_file), Context.MODE_PRIVATE)
        val userId: String? = prefs?.getString("id", null)
        post = InterestedOnFragmentArgs.fromBundle((requireArguments())).Post
        activity?.runOnUiThread(java.lang.Runnable {
            GlobalScope.launch(Dispatchers.Main) {
                try {
                    users = PostService.getPostsInterested(userId!!, post.id!!)!!
                    val recInterested = v.findViewById<View>(R.id.rv_interested) as RecyclerView
                    val adapter = UserAdapter(users) {position ->onItemClick(position)}
                    recInterested.adapter = adapter
                    recInterested.layoutManager = LinearLayoutManager(context)

                }catch(error: Exception){
                    Toast.makeText(v.context, "Error de conexión con el servidor. Intente mas tarde.", Toast.LENGTH_SHORT).show()
                }
            }
        })
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        v = inflater.inflate(R.layout.interested_on_fragment, container, false)
        return v
    }

    fun onItemClick(position: Int) {
        Log.d("HICISTE CLICK:", "TE FELICITO $position")
        //TODO ACA VA EL REDIRECCIONAMIENTO AL FRAGMENTO DE CONFIRMAR ADOPCION QUE NO ESTA MERGEADO AUN
        var action: NavDirections? = null
            action = InterestedOnFragmentDirections.actionInterestedOnFragmentToPostConfirmationFragment(users[position], post)
        v.findNavController().navigate(action)

    }
}