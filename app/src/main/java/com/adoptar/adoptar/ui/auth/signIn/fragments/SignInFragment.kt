package com.adoptar.adoptar.ui.auth.signIn.fragments

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.findNavController
import com.adoptar.adoptar.MainActivity
import com.adoptar.adoptar.R
import com.adoptar.adoptar.modules.users.domain.User
import com.adoptar.adoptar.modules.users.domain.UserSimply
import com.adoptar.adoptar.modules.users.infrastructure.UserServices
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthCredential
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import kotlinx.android.synthetic.main.sign_in_fragment.*
import kotlinx.android.synthetic.main.sign_in_fragment.emailEditText
import kotlinx.android.synthetic.main.sign_in_fragment.passwordEditText
import kotlinx.android.synthetic.main.sign_in_fragment.signUpButton
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import okhttp3.internal.userAgent


class SignInFragment : Fragment() {
    lateinit var v: View
    private val GOOGLE_SIGN_IN = 100


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.sign_in_fragment, container, false)
        return v
    }

    override fun onStart() {
        super.onStart()
        session()
        setup()
    }

    private fun setup() {
        signInButton.setOnClickListener {
            if (emailEditText.text.isNotEmpty() && passwordEditText.text.isNotEmpty()) {
                FirebaseAuth.getInstance().signInWithEmailAndPassword(
                    emailEditText.text.toString(),
                    passwordEditText.text.toString()
                ).addOnCompleteListener {
                    if (it.isSuccessful) {
                        // TODO mandar al back mail e id (it.getResult().user.uid y it.getResult().user.email)
                        GlobalScope.launch(Dispatchers.Main) {
                            val user = UserServices.userById(it.result.user!!.uid)!!
                            if (user.isFirstTime!!) {
                                showAditional(user)
                            } else {
                                showHome(user.email!!, user.id!!, user.displayName!!)
                            }
                        }
                    } else {
                        Log.w("TAG", "createUserWithEmail:failure", it.exception)
                        Toast.makeText(
                            v.context, it.exception?.message ?: "Error en el servidor",
                            Toast.LENGTH_LONG
                        ).show()
                        cleanFields()
                    }
                }
            }
        }

        signUpButton.setOnClickListener {
            val action = SignInFragmentDirections.actionSignInFragmentToSignUpFragment()
            v.findNavController().navigate(action)
        }

        googleButton.setOnClickListener {
            // configuracion
            val googleConf: GoogleSignInOptions =
                GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestIdToken(getString(R.string.default_web_client_id))
                    .requestEmail()
                    .build()

            val googleClient: GoogleSignInClient = GoogleSignIn.getClient(v.context, googleConf)
            googleClient.signOut()
            startActivityForResult(googleClient.signInIntent, GOOGLE_SIGN_IN)
        }

    }

    private fun showHome(email: String, id: String, displayName: String) {
        val mainIntent: Intent = Intent(v.context, MainActivity::class.java).apply() {
            putExtra("email", email)
            putExtra("id", id)
            putExtra("displayName", displayName)
        }
        startActivity(mainIntent)
    }

    private fun showAditional(user: User) {
        var action = SignInFragmentDirections.actionSignInFragmentToAditionalFragment(user)
        v.findNavController().navigate(action)
    }

    private fun cleanFields() {
        emailEditText.text.clear()
        passwordEditText.text.clear()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == GOOGLE_SIGN_IN) {
            val task: Task<GoogleSignInAccount> = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                val account: GoogleSignInAccount? = task.getResult(ApiException::class.java)
                if (account != null) {
                    val credential: AuthCredential =
                        GoogleAuthProvider.getCredential(account.idToken, null)
                    FirebaseAuth.getInstance().signInWithCredential(credential)
                        .addOnCompleteListener {
                            if (it.isSuccessful) {
                                GlobalScope.launch(Dispatchers.Main) {
                                    val user = UserServices.userById(it.result.user!!.uid!!)
                                    if (user != null) {
                                        if (user.isFirstTime!!) {
                                            showAditional(user)
                                        } else {
                                            showHome(user.email!!, user.id!!, user.displayName!!)
                                        }
                                    } else {
                                        val userPost = UserSimply(
                                            it.result.user!!.email!!, it.result.user!!.uid!!)
                                        UserServices.userPost(userPost)
                                        val user = UserServices.userById(it.result.user!!.uid!!) !!
                                        showAditional(user)
                                    }
                                }
                            } else {
                                Log.w("TAG", "createUserWithEmail:failure", it.exception)
                                Toast.makeText(
                                    v.context, "Authentication failed.",
                                    Toast.LENGTH_LONG
                                ).show()
                            }
                        }
                }
            } catch (e: ApiException) {
                Log.d("error", e.toString())
            }
        }
    }

    private fun session() {
        val prefs: SharedPreferences? =
            activity?.getSharedPreferences(getString(R.string.prefs_file), Context.MODE_PRIVATE)
        val displayName: String? = prefs?.getString("displayName", null)
        val email: String? = prefs?.getString("email", null)
        val id: String? = prefs?.getString("id", null)
        val user: User? = null
        user?.displayName = displayName
        user?.email = email
        user?.id = id
        if (displayName != null && email != null && id != null) {
            authLayout.visibility = View.INVISIBLE
            showHome(email, id, displayName)
        }
    }
}






