package com.adoptar.adoptar.ui.posts.models

import android.os.Parcel
import android.os.Parcelable
import com.adoptar.adoptar.ui.user.models.User
import java.util.*


class Post(var pet: Pet, var user: User, var postType: String, var description: String, var follow: Boolean, var urlImages: Array<String>, var title: String, var date: String, var timeAgo : String?=null) :


    Parcelable {

    constructor(parcel: Parcel) : this(
        TODO("pet"),
        TODO("user"),
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readByte() != 0.toByte()!!,
        parcel.createStringArray()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()
    )

    companion object {

        fun createPetsList(pet: Pet,user : User, numPosts: Int): ArrayList<Post> {
            val posts = ArrayList<Post>()
            for (i in 1..numPosts) {
                posts.add(
                    Post(
                        pet,
                        user,
                        "ADOPCION",
                        "Es un perro de malo",
                        true,
                        arrayOf("https://www.denverpost.com/wp-content/uploads/2019/06/AP19172641725831.jpg",
                            "https://animalesmascotas.com/wp-content/uploads/2009/03/tipos-de-bulldogs-frances-ingls-y-americano-bulldog-ingles-e1389363595472.jpg",
                            "https://i.pinimg.com/originals/ef/6c/7a/ef6c7ac15ff0eb9a46cb6e3b2bf2cbd1.jpg"),
                        "Max en adopcion",
                        "2020-09-02T15:35:25"
                    )
                )
            }
            return posts
        }

        val CREATOR: Parcelable.Creator<Post> = object : Parcelable.Creator<Post> {
            override fun createFromParcel(source: Parcel): Post = Post(source)
            override fun newArray(size: Int): Array<Post?> = arrayOfNulls(size)
        }
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(description)
        parcel.writeByte(if (follow) 1 else 0)
        parcel.writeStringArray(urlImages)
        parcel.writeString(title)
    }

    override fun describeContents(): Int {
        return 0
    }



}


