package com.adoptar.adoptar.ui.posts.fragments

import android.app.AlertDialog
import android.content.Context
import android.content.SharedPreferences
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.navigation.findNavController
import com.adoptar.adoptar.R
import com.adoptar.adoptar.R.color.adp_primary
import com.adoptar.adoptar.modules.messages.domain.ReviewPost
import com.adoptar.adoptar.modules.messages.infrastructure.ReviewServices
import com.adoptar.adoptar.ui.posts.viewModels.PostDetailViewModel
import com.google.android.datatransport.runtime.util.PriorityMapping.toInt
import com.squareup.picasso.Picasso
import com.synnapps.carouselview.CarouselView
import com.synnapps.carouselview.ImageListener
import kotlinx.android.synthetic.main.fragment_post_confirmation.*
import kotlinx.android.synthetic.main.popup_review_fragment.view.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeFormatterBuilder
import java.util.ArrayList

class PostDetailFragment : Fragment() {

    lateinit var post: com.adoptar.adoptar.modules.posts.domain.Post
    private var cameFromHome: Boolean = false

    private var fotos: ArrayList<String> = ArrayList<String>()
    private lateinit var textUserName: TextView
    private lateinit var textPetName: TextView
    private lateinit var textPetSex: TextView
    private lateinit var textPetType: TextView
    private lateinit var textPetAge: TextView
    private lateinit var textPetSize: TextView
    private lateinit var textPetBreed: TextView
    private lateinit var textPetNeutered: TextView
    private lateinit var textPetWeight: TextView
    private lateinit var textPostDescription: TextView
    private lateinit var textUserUbication: TextView
    private lateinit var buttonQuierAdoptar: Button
    private lateinit var buttonVerSeguimiento: Button
    private lateinit var buttonResenarAdoptante: Button
    private lateinit var buttonResenarGuardian: Button
    private lateinit var buttonQuestions: ImageButton
    private lateinit var textPetDate: TextView
    private lateinit var linearUser: LinearLayout
    private lateinit var idActiveUser: String
    private lateinit var popupReview: View
    private lateinit var popupReviewEditText: EditText
    private lateinit var popupReviewBtnReseñar: Button
    private lateinit var popupReviewBtnCantelar: Button
    private lateinit var btnEditPost: ImageButton
    private lateinit var ly_adoptedUser: LinearLayout
    private lateinit var textAdoptedName: TextView
    private lateinit var textAdoptedMail: TextView
    private lateinit var textAdoptedPhone: TextView
    private lateinit var ly_createdUser: LinearLayout
    private lateinit var textCreatedName: TextView
    private lateinit var textCreatedMail: TextView
    private lateinit var textCreatedPhone: TextView

    companion object {
        fun newInstance() = PostDetailFragment()
    }

    private lateinit var viewModel: PostDetailViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val v = inflater.inflate(R.layout.post_detail_fragment, container, false)
        val prefs: SharedPreferences? =
            activity?.getSharedPreferences(getString(R.string.prefs_file), Context.MODE_PRIVATE)
        idActiveUser = prefs?.getString("id", null).toString()

        post = PostDetailFragmentArgs.fromBundle(requireArguments()).Post
        cameFromHome = PostDetailFragmentArgs.fromBundle(requireArguments()).cameFromHome
        Log.d("foto del post", post.imageFiles.toString())
        fotos = post.imageFiles!!
//        fotos.add(post.imageFiles.toString())
//        fotos.add(post.imageFiles.toString())
//        fotos.add(post.imageFiles.toString())

        linearUser = v.findViewById(R.id.user_post)
        linearUser.setOnClickListener {
            val action =
                PostDetailFragmentDirections.actionPostDetailFragmentToUserPublicFragment(post.createdBy!!)
            v.findNavController().navigate(action)
        }

        btnEditPost = v.findViewById(R.id.btnEditPost)

        textUserName = v.findViewById(R.id.textUserName)
        textUserName.text = post.createdBy?.displayName

        textPetName = v.findViewById(R.id.textPetName)
        textPetName.text = post.petName

        textPetSex = v.findViewById(R.id.textPetSex)
        textPetSex.text = post.petSex

        textPetType = v.findViewById(R.id.textPetType)
        textPetType.text = post.petType

        textPetAge = v.findViewById(R.id.textPetAge)
        textPetAge.text = post.petAge

        textPetSize = v.findViewById(R.id.textPetSize)
        textPetSize.text = post.petSize

        textPetBreed = v.findViewById(R.id.textPetBreed)
        textPetBreed.text = post.petBreed

        val listaFecha = post.createdAt?.split("T")
        textPetDate = v.findViewById(R.id.textPetDate)
        textPetDate.text = listaFecha?.get(0)

        textPetNeutered = v.findViewById(R.id.textPetNeutered)
        textPetNeutered.text = post.petCastrated

        textPetWeight = v.findViewById(R.id.textPetWeight)
        textPetWeight.text = post.petWeigth

        textPostDescription = v.findViewById(R.id.textPostDescription)
        textPostDescription.text = post.postDescription

        textUserUbication = v.findViewById(R.id.textUserUbication)
        textUserUbication.text = post.userId

        buttonQuierAdoptar = v.findViewById(R.id.buttonQuierAdoptar)
        buttonQuierAdoptar.setBackgroundColor(ContextCompat.getColor(v.context, adp_primary))

        buttonResenarAdoptante = v.findViewById(R.id.buttonReseñarAdoptante)
        buttonVerSeguimiento = v.findViewById(R.id.buttonVerSeguimiento)
        buttonResenarGuardian = v.findViewById(R.id.buttonReseñarGuardian)

        textAdoptedName = v.findViewById(R.id.textAdoptedName)
        textAdoptedName.text = post.adoptedBy?.displayName

        textAdoptedMail = v.findViewById(R.id.textAdoptedMail)
        textAdoptedMail.text = post.adoptedBy?.email

        textAdoptedPhone = v.findViewById(R.id.textAdoptedPhone)
        textAdoptedPhone.text = post.adoptedBy?.phoneNumber

        ly_adoptedUser = v.findViewById(R.id.ly_adoptedUser)

        ly_adoptedUser.setOnClickListener {
            val action =
                PostDetailFragmentDirections.actionPostDetailFragmentToUserPublicFragment(post.adoptedBy!!)
            v.findNavController().navigate(action)
        }

        textCreatedName = v.findViewById(R.id.textCreatedName)
        textCreatedName.text = post.createdBy?.displayName

        textCreatedMail = v.findViewById(R.id.textCreatedMail)
        textCreatedMail.text = post.createdBy?.email

        textCreatedPhone = v.findViewById(R.id.textCreatedPhone)
        textCreatedPhone.text = post.createdBy?.phoneNumber

        ly_createdUser = v.findViewById(R.id.ly_createdUser)

        ly_createdUser.setOnClickListener {
            val action =
                PostDetailFragmentDirections.actionPostDetailFragmentToUserPublicFragment(post.createdBy!!)
            v.findNavController().navigate(action)
        }


        if (post.adoptedBy?.id != null) {
            if (idActiveUser == post.adoptedBy?.id) {
                ly_createdUser.visibility = View.VISIBLE
                ly_adoptedUser.visibility = View.GONE
            } else if (idActiveUser == post.createdBy?.id) {
                ly_adoptedUser.visibility = View.VISIBLE
                ly_createdUser.visibility = View.GONE
            }
        } else {
            ly_adoptedUser.visibility = View.GONE
            ly_createdUser.visibility = View.GONE
        }

        buttonVisibilityLoad()


        /*if(!cameFromHome){
             buttonQuierAdoptar.visibility = View.INVISIBLE
        }*/

        val carouselView = v.findViewById(R.id.carouselView) as CarouselView;
        carouselView.pageCount = fotos.size;
        carouselView.setImageListener(imageListener);
        buttonQuierAdoptar.setOnClickListener {
            val action =
                PostDetailFragmentDirections.actionPostDetailFragmentToAdoptionFormFragment(post.id!!)
            v.findNavController().navigate(action)
        }

        buttonVerSeguimiento.setOnClickListener {
            val action =
                PostDetailFragmentDirections.actionPostDetailFragmentToMonitoringFragment(post.id!!)
            v.findNavController().navigate(action)
        }

        v.findViewById<ImageButton>(R.id.btnEditPost).setOnClickListener {
            val action =
                PostDetailFragmentDirections.actionPostDetailFragmentToNewPostFragment(post, true)
            v.findNavController().navigate(action)
        }

        buttonQuestions = v.findViewById(R.id.buttonQuestions)
        buttonQuestions.setOnClickListener {
            val action =
                PostDetailFragmentDirections.actionPostDetailFragmentToPostQuestionsFragment(post)
            v.findNavController().navigate(action)
        }

        buttonResenarAdoptante.setOnClickListener {
            var dialog = AlertDialog.Builder(context).create()
            popupReview = layoutInflater.inflate(R.layout.popup_review_fragment, null)
            dialog.setView(popupReview)
            dialog.setTitle("Reseñar adoptante")
            dialog.show()
            popupReview.buttonResenar.setOnClickListener {
                popupReviewEditText = popupReview.editTextResena

                GlobalScope.launch(Dispatchers.Main) {
                    try {
                        if (popupReviewEditText.text.toString() != "") {
                            ReviewServices.reviewPost(
                                ReviewPost(
                                    idActiveUser,
                                    post.adoptedBy?.id,
                                    popupReviewEditText.text.toString()
                                )
                            )
                            popupReviewEditText.text.clear()
                            dialog.dismiss();
                            Toast.makeText(
                                v.context,
                                "Reseña creada exitosamente",
                                Toast.LENGTH_SHORT
                            ).show()
                        } else {
                            dialog.dismiss();
                            Toast.makeText(
                                v.context,
                                "No se pueden crear reseñas vacías !!!",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    } catch (error: Exception) {
                        Log.i("error: ", error.message)
                        Toast.makeText(
                            v.context,
                            "Error de conexión con el servidor. Intente mas tarde.",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
            }
        }

        buttonResenarGuardian.setOnClickListener {
            var dialog = AlertDialog.Builder(context).create()
            popupReview = layoutInflater.inflate(R.layout.popup_review_fragment, null)
            dialog.setView(popupReview)
            dialog.setTitle("Reseñar guardian")
            dialog.show()
            popupReview.buttonResenar.setOnClickListener {
                popupReviewEditText = popupReview.editTextResena

                GlobalScope.launch(Dispatchers.Main) {
                    try {
                        if (popupReviewEditText.text.toString() != "") {
                            ReviewServices.reviewPost(
                                ReviewPost(
                                    idActiveUser,
                                    post.createdBy?.id,
                                    popupReviewEditText.text.toString()
                                )
                            )
                            popupReviewEditText.text.clear()
                            dialog.dismiss();
                            Toast.makeText(
                                v.context,
                                "Reseña creada exitosamente",
                                Toast.LENGTH_SHORT
                            ).show()
                        } else {
                            dialog.dismiss();
                            Toast.makeText(
                                v.context,
                                "No se pueden crear reseñas vacías !!!",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    } catch (error: Exception) {
                        Toast.makeText(
                            v.context,
                            "Error de conexión con el servidor. Intente mas tarde.",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }

            }
        }

        return v
    }

    var imageListener: ImageListener = object : ImageListener {
        override fun setImageForPosition(position: Int, imageView: ImageView) {
            // You can use Glide or Picasso here
            Picasso.get().load(fotos[position]).into(imageView)
        }
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(PostDetailViewModel::class.java)
        // TODO: Use the ViewModel
    }

    private fun buttonVisibilityLoad() {

        if (post.adoptedBy?.id != null) {
            buttonQuierAdoptar.visibility = View.GONE
            if ((idActiveUser == post.createdBy?.id) || (idActiveUser == post.adoptedBy?.id)) {
                if (post.petMonitoring) {
                    buttonVerSeguimiento.visibility = View.VISIBLE
                    if (idActiveUser == post.createdBy?.id) {
                        buttonResenarAdoptante.visibility = View.VISIBLE
                    } else {
                        buttonResenarGuardian.visibility = View.VISIBLE
                    }
                } else {
                    if (idActiveUser == post.createdBy?.id) {
                        buttonResenarAdoptante.visibility = View.VISIBLE

                    } else {
                        buttonResenarGuardian.visibility = View.VISIBLE
                    }
                }

            }
        } else if (post.createdBy?.id == idActiveUser) {
            buttonQuierAdoptar.visibility = View.GONE
            btnEditPost.visibility = View.VISIBLE
        }
    }
}