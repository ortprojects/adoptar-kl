package com.adoptar.adoptar.ui.posts.fragments

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.navigation.findNavController
import com.adoptar.adoptar.MainActivity
import com.adoptar.adoptar.R
import com.adoptar.adoptar.modules.posts.domain.FormAdoption
import com.adoptar.adoptar.modules.posts.infrastructure.PostService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class AdoptionFormFragment : Fragment() {

    lateinit var v: View
    lateinit var btnSend: Button
    lateinit var aboutPeople: EditText
    lateinit var aboutPet: EditText
    lateinit var aboutCastrated: EditText
    lateinit var hasNoPet: EditText
    lateinit var aboutLocation: EditText
    lateinit var postId: String

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        postId = AdoptionFormFragmentArgs.fromBundle(requireArguments()).postId
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.adoption_form_fragment, container, false)

        aboutPeople = v.findViewById(R.id.question_one)
        aboutPet = v.findViewById(R.id.question_two)
        aboutCastrated = v.findViewById(R.id.question_three)
        hasNoPet = v.findViewById(R.id.question_four)
        aboutLocation = v.findViewById(R.id.question_five)
        btnSend = v.findViewById(R.id.btnSend)
        btnSend.setOnClickListener {
            completeForm()
        }
        return v
    }

    override fun onStart() {
        super.onStart()
    }


    private fun completeForm() {
        val prefs: SharedPreferences? =
            activity?.getSharedPreferences(getString(R.string.prefs_file), Context.MODE_PRIVATE)
        val userId: String? = prefs?.getString("id", null)
        if (validatedFields()) {
            var formAdoption = FormAdoption(aboutPeople.text.toString(),  aboutPet.text.toString(), aboutCastrated.text.toString(),
                 aboutLocation.text.toString(), hasNoPet.text.toString(), userId)
            try{
                GlobalScope.launch(Dispatchers.Main){
                    PostService.addFormAdoption(formAdoption, postId)
                    Toast.makeText(v.context, "Solicitud de adopción enviada exitosamente.", Toast.LENGTH_SHORT).show()
                    val action = AdoptionFormFragmentDirections.actionAdoptionFormFragmentToNavigationHome()
                    v.findNavController().navigate(action)
                }
            }catch(error: Exception){
                Toast.makeText(v.context, "Error de conexión con el servidor. Intente mas tarde.", Toast.LENGTH_SHORT).show()
            }

        } else {
            Toast.makeText(v.context, "Debes completar todas las preguntas", Toast.LENGTH_SHORT)
                .show()
        }
    }

    private fun validatedFields(): Boolean {
        return !(aboutPeople.text.isEmpty() ||
                aboutPet.text.isEmpty() ||
                aboutCastrated.text.isEmpty() ||
                hasNoPet.text.isEmpty() ||
                aboutLocation.text.isEmpty())
    }
}