package com.adoptar.adoptar.ui.user.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.adoptar.adoptar.R
import com.adoptar.adoptar.ui.home.viewModels.HomeViewModel


class Publish_rv_Fragment : Fragment() {
    private lateinit var homeViewModel: HomeViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        homeViewModel =
            ViewModelProviders.of(this).get(HomeViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_publish_rv_, container, false)

        homeViewModel.text.observe(viewLifecycleOwner, Observer {

        })
        return root
    }
}