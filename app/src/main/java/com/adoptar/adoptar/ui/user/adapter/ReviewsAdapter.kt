package com.adoptar.adoptar.ui.user.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.adoptar.adoptar.R
import com.adoptar.adoptar.modules.messages.domain.Review
import com.adoptar.adoptar.modules.messages.domain.ReviewResponsePost
import com.adoptar.adoptar.modules.messages.infrastructure.ReviewServices
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class ReviewsAdapter(
    private val reviews: MutableList<Review>,
    private val context: Context?,
    private val v: View,
    private val idActiveUser: String
) : RecyclerView.Adapter<ReviewsAdapter.ViewHolder>()
{
    // Provide a direct reference to each of the views within a data item
    // Used to cache the views within the item layout for fast access
    inner class ViewHolder(listItemView: View) : RecyclerView.ViewHolder(listItemView) {
        // Your holder should contain and initialize a member variable
        // for any view that will be set as you render a row
        val textReview: TextView?  = itemView.findViewById<TextView>(R.id.textReview)
        val textReply: TextView? = itemView.findViewById<TextView>(R.id.textReply)
        val editTextReply : EditText = itemView.findViewById<EditText>(R.id.editTextReply)
        val btnSubmitReply: Button = itemView.findViewById<Button>(R.id.btnSubmitReply)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)
        // Inflate the custom layout
        val reviewsView = inflater.inflate(R.layout.item_review_fragment, parent, false)
        // Return a new holder instance
        return ViewHolder(reviewsView)
    }

    // Involves populating data into the item through holder
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        // Get the data model based on position
        val reviewsAux: Review = reviews[position]
        // Set item views based on your views and data model
        val textReview = viewHolder.textReview
        textReview?.text = reviewsAux.text // pregunta
        val textReply = viewHolder.textReply
        val editTextReply = viewHolder.editTextReply
        val btnSubmitReply = viewHolder.btnSubmitReply

        if (reviewsAux.userId != idActiveUser){
            editTextReply.visibility = View.GONE
            btnSubmitReply.visibility = View.GONE
        }

        if (reviewsAux.response?.text != null) {
            editTextReply.visibility = View.GONE
            btnSubmitReply.visibility = View.GONE
            textReply?.text = reviewsAux.response.text // answer
        }
        btnSubmitReply.setOnClickListener {
            GlobalScope.launch(Dispatchers.Main) {
                try{
                    onItemClick(reviewsAux, editTextReply, viewHolder)
                    Toast.makeText(v.context, "Respuesta creada exitosamente", Toast.LENGTH_SHORT).show()
                }catch(error: Exception){
                    Log.i("error: ", error.message)
                    Toast.makeText(v.context, "Error de conexión con el servidor. Intente mas tarde.", Toast.LENGTH_SHORT).show()
                }
            }

            textReply?.text = editTextReply.text.toString()
            editTextReply.visibility = View.GONE
            btnSubmitReply.visibility = View.GONE
        }

    }

    // Returns the total count of items in the list
    override fun getItemCount(): Int {
        return reviews.size
    }

    suspend fun onItemClick(
        review: Review,
        editTextReply: EditText,
        viewHolder: ViewHolder
    ) {
        ReviewServices.responseToReviewPost(ReviewResponsePost(idActiveUser, editTextReply.text.toString()), idActiveUser, review.id.toString())
        //hideKeyboard(viewHolder)
    }

    private fun hideKeyboard(viewHolder: ViewHolder) {
        val inputManager = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputManager.hideSoftInputFromWindow(v.windowToken, 0)
    }
}
