package com.adoptar.adoptar.ui.posts.fragments

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.Color
import android.location.Geocoder
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.text.Editable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.core.content.PermissionChecker.checkSelfPermission
import androidx.fragment.app.Fragment
import androidx.loader.content.CursorLoader
import androidx.navigation.findNavController
import com.adoptar.adoptar.R
import com.adoptar.adoptar.modules.posts.domain.Post
import com.adoptar.adoptar.modules.posts.domain.PostCreate
import com.adoptar.adoptar.modules.posts.infrastructure.PostService
import com.google.android.gms.common.api.Status
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.api.net.PlacesClient
import com.google.android.libraries.places.widget.AutocompleteSupportFragment
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener
import kotlinx.android.synthetic.main.new_post_fragment.*
import kotlinx.android.synthetic.main.new_post_fragment.view.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File
import java.util.*
import kotlin.collections.ArrayList


class NewPostFragment : Fragment() {

    lateinit var postArgs: NewPostFragmentArgs
    lateinit var post: Post
    private var title: String? = null
    private var name: String? = null
    private var petType: String? = null
    private var sex: String? = null
    private var publicationType: String? = null
    private var race: String? = null
    private var age: String? = null
    private var size: String? = null
    private var weight: String? = null
    private var castrated: String? = null
    private var description: String? = null
    private var tracing: String? = null
    private var arePhotosLoaded: Boolean = false;
    private var validationError: String? = ""
    private var latitude: Double = 0.0;
    private var longitude: Double = 0.0;
    var address: Place? = null
    lateinit var placesClient: PlacesClient
    var placeFields = Arrays.asList(
        Place.Field.ID,
        Place.Field.NAME,
        Place.Field.ADDRESS,
        Place.Field.LAT_LNG
    )
    lateinit var v: View
    lateinit var txtTitle: EditText
    lateinit var txtInputName: EditText
    lateinit var rgAnimal: RadioGroup
    lateinit var rBtnDog: RadioButton
    lateinit var rBtnCat: RadioButton
    lateinit var rgSex: RadioGroup
    lateinit var rBtnMale: RadioButton
    lateinit var rBtnFemale: RadioButton
    lateinit var rgPublicationType: RadioGroup
    lateinit var rBtnLost: RadioButton
    lateinit var rBtnAdoption: RadioButton
    lateinit var txtInputRace: EditText
    lateinit var rgAge: RadioGroup
    lateinit var rBtnPuppy: RadioButton
    lateinit var rBtnYoung: RadioButton
    lateinit var rBtnAdult: RadioButton
    lateinit var rBtnVeteran: RadioButton
    lateinit var txtInputWeigth: EditText
    lateinit var rgCastrated: RadioGroup
    lateinit var rBtnCastratedYes: RadioButton
    lateinit var rBtnCastratedNo: RadioButton
    lateinit var rBtnCastratedNoIdea: RadioButton
    lateinit var txtInputDescription: EditText
    lateinit var rgTracing: RadioGroup
    lateinit var rgSize: RadioGroup
    lateinit var rBtnSmall: RadioButton
    lateinit var rBtnMedium: RadioButton
    lateinit var rBtnBig: RadioButton
    lateinit var btnTracingYes: RadioButton
    lateinit var btnTracingNo: RadioButton
    var isForEdit: Boolean = false

    //TODO RESOLVER COMO USAR LA DIRECCION
    lateinit var btnChoose: Button
    lateinit var btnSubmit: Button
    lateinit var photosLayout: LinearLayout
    lateinit var textView11: TextView
    var stringPath: MutableList<String> = ArrayList<String>()
    var multipartArray: MutableList<MultipartBody.Part?> = ArrayList<MultipartBody.Part?>()
    lateinit var imagePath: String


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    @SuppressLint("WrongConstant")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        v = inflater.inflate(R.layout.new_post_fragment, container, false)

        postArgs = NewPostFragmentArgs.fromBundle(requireArguments())
        post = postArgs.Post
        isForEdit = postArgs.isForEdit
        setElementsVariables()

        btnSubmit.setOnClickListener {
            if (validateForm()) {
                generateMultiPart()
                val userCreate = generatePostCreate()
                try {
                    GlobalScope.launch(Dispatchers.Main) {
                        var resultado = ""
                        if(isForEdit){
                            resultado = PostService.updatePost(userCreate, post.id!!)
                            resultado = "Publicacion modificada exitosamente."
                        }
                        else {
                            resultado = PostService.addPost(multipartArray.toList(), userCreate)
                            resultado = "Publicacion realizada exitosamente."
                        }
                        Toast.makeText(
                            v.context,
                            resultado,
                            Toast.LENGTH_LONG
                        ).show()
                        val action =
                            NewPostFragmentDirections.actionNewPostFragmentToNavigationHome()
                        v.findNavController().navigate(action)
                    }
                } catch (error: Exception) {
                    Toast.makeText(
                        v.context,
                        "Error de conexión con el servidor. Intente mas tarde.",
                        Toast.LENGTH_LONG
                    ).show()
                }

            } else {
                Toast.makeText(
                    v.context, validationError,
                    Toast.LENGTH_LONG
                ).show()
                validationError = ""
            }
        }

        if (post.title == "") { //ACA ENTRA SI ES UN POST A CREAR
            arePhotosLoaded = false;

            btnChoose.setBackgroundColor(ContextCompat.getColor(v.context, R.color.adp_primary))
            btnChoose.setOnClickListener {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    //preguntamos si tiene permiso a la galeria
                    if (checkSelfPermission(
                            v.context,
                            Manifest.permission.READ_EXTERNAL_STORAGE
                        ) == PackageManager.PERMISSION_DENIED
                    ) {
                        //Pedir permiso al usuario
                        val permisoArchivos = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE)
                        requestPermissions(permisoArchivos, 200)
                    } else {
                        // entonces tiene permiso
                        startFileChooser()
                    }
                } else {
                    // tiene version de Lollipop hacia abajo y por default tiene permiso
                    startFileChooser()
                }
            }

            btnSubmit.setBackgroundColor(ContextCompat.getColor(v.context, R.color.adp_primary))

        } else if (post.title != "") { //ACA ENTRA SI ES UN POST A EDITAR
            photosLayout.visibility = View.GONE
            btnChoose.visibility = View.GONE
            textView11.visibility = View.GONE
            btnSubmit.text = "Aplicar Cambios"
            btnSubmit.setBackgroundColor(ContextCompat.getColor(v.context, R.color.adp_primary))

            setInfoInElements()
        }

        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupPlacesAutocomplete()
    }

    override fun onStart() {
        super.onStart()
        initPlaces()

    }

    private fun startFileChooser() {
        var intent = Intent(Intent.ACTION_PICK)
        //intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)
        //intent.addCategory(Intent.CATEGORY_OPENABLE)
        intent.type = "image/*"
        startActivityForResult(intent, 200);
    }

    private fun generatePostCreate(): PostCreate {
        val prefs: SharedPreferences? =
            activity?.getSharedPreferences(getString(R.string.prefs_file), Context.MODE_PRIVATE)
        val userId: String? = prefs?.getString("id", null)
        return PostCreate(
            userId!!,
            name!!,
            sex!!,
            petType!!,
            publicationType!!,
            race!!,
            age!!,
            size!!,
            weight!!,
            castrated!!,
            description!!,
            tracing!!,
            latitude!!,
            longitude!!,
            title!!
        )
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK && requestCode == 200) {
            photosLayout.visibility = View.VISIBLE

            // Cuando hay mas de una foto se crea un clipData
            if (data?.clipData != null) {
                var count = data.clipData?.itemCount
                if (count != null) {
                    if (count <= 5) {
                        //cleanImageViews()
                        for (i in 0 until count) {
                            var imageUri: Uri = data.clipData!!.getItemAt(i).uri
                            stringPath.add(obtenerPathReal(imageUri))
                            Log.d("SITRINGPATHHHHHHHHH", stringPath.toString())
                            //imagePath = obtenerPathReal(imageUri)
                            if (imageView.drawable == null) {
                                imageView.setImageURI(imageUri)
                            } else if (imageView1.drawable == null) {
                                imageView1.setImageURI(imageUri)
                            } else if (imageView2.drawable == null) {
                                imageView2.setImageURI(imageUri)
                            } else if (imageView3.drawable == null) {
                                imageView3.setImageURI(imageUri)
                            } else if (imageView4.drawable == null) {
                                imageView4.setImageURI(imageUri)
                            }
//                                0 -> imageView.setImageURI(imageUri)
//                                1 -> imageView1.setImageURI(imageUri)
//                                2 -> imageView2.setImageURI(imageUri)
//                                3 -> imageView3.setImageURI(imageUri)
//                                4 -> imageView4.setImageURI(imageUri)
//
                        }
                        arePhotosLoaded = true;
                    } else {
                        if (!arePhotosLoaded) {
                            photosLayout.visibility = View.GONE;
                        }
                        Toast.makeText(
                            context, "Se permiten hasta 5 fotos",
                            Toast.LENGTH_LONG
                        ).show()
                    }
                }
                // Cuando hay una sola foto no se crea el clipData
            } else if (data?.data != null) {
                cleanImageViews()
                arePhotosLoaded = true;
                var imageUri: Uri = data.data!!
                Log.d("uriiiiiiiiiiiiii", imageUri.toString())

                imageView.setImageURI(imageUri)
            }
        }
    }

    companion object {
        fun newInstance() = NewPostFragment()
    }

    private fun cleanImageViews() {
        imageView.setImageURI(null)
        imageView1.setImageURI(null)
        imageView2.setImageURI(null)
        imageView3.setImageURI(null)
        imageView4.setImageURI(null)
    }

    //obtener path real de la image a traves del uri
    private fun obtenerPathReal(uri: Uri): String {
        var resultado = ""
        val projection = arrayOf(MediaStore.Images.Media.DATA)
        val loader: CursorLoader = CursorLoader(v.context, uri, projection, null, null, null)
        val cursor: Cursor? = loader.loadInBackground()
        val column_idx = cursor?.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
        cursor?.moveToFirst()
        resultado = cursor!!.getString(column_idx!!)
        cursor.close()
        return resultado
    }

    private fun generateMultiPart() {
        multipartArray = ArrayList<MultipartBody.Part?>()
//        for (x in 0..5){
//            multipartArray.add(null)
//        }
        stringPath.forEach() { elemento ->
            val file = File(elemento)
            val requestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), file)
            val body = MultipartBody.Part.createFormData("imageFiles", file.name, requestBody)
            multipartArray.add(body)
        }

//        stringPath.forEachIndexed  {index, elemento ->
//            val file = File(elemento)
//            val requestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), file)
//            val body = MultipartBody.Part.createFormData("imageFiles", file.name, requestBody)
//            multipartArray[index] = body
        // }
    }


    private fun validateForm(): Boolean {
        var resultado = true

        title = v.findViewById<EditText>(R.id.txtTitle).text.toString()
        if (this.title == "") {
            validationError += "Debe ingresar un título"
            resultado = false
        }

        this.name = v.findViewById<EditText>(R.id.txtInputName).text.toString()
        if (this.name == "") {
            validationError += "Debe ingresar un nombre"
            resultado = false
        }

        when (rgAnimal.checkedRadioButtonId.toString()) {
            rBtnDog.id.toString() -> this.petType = "Perro"
            rBtnCat.id.toString() -> this.petType = "Gato"
            else -> {
                validationError += "Debe seleccionar un tipo de animal"
                resultado = false
            }
        }

        when (rgSex.checkedRadioButtonId.toString()) {
            rBtnMale.id.toString() -> this.sex = "Macho"
            rBtnFemale.id.toString() -> this.sex = "Hembra"
            else -> {
                validationError += "Debe seleccionar un sexo"
                resultado = false
            }
        }

        when (rgPublicationType.checkedRadioButtonId.toString()) {
            rBtnLost.id.toString() -> this.publicationType = "Perdido"
            rBtnAdoption.id.toString() -> this.publicationType = "Adopción"
            else -> {
                validationError += "Debe seleccionar un tipo de publicación"
                resultado = false
            }
        }

        this.race = v.findViewById<EditText>(R.id.txtInputRace).text.toString()
        if (this.race == "") {
            validationError += "Debe ingresar una raza"
            resultado = false
        }

        when (rgAge.checkedRadioButtonId.toString()) {
            rBtnPuppy.id.toString() -> this.age = "Cachorro"
            rBtnYoung.id.toString() -> this.age = "Joven"
            rBtnAdult.id.toString() -> this.age = "Adulto"
            rBtnVeteran.id.toString() -> this.age = "Veterano"
            else -> {
                validationError += "Debe seleccionar una edad"
                resultado = false
            }
        }

        when (rgSize.checkedRadioButtonId.toString()) {
            rBtnSmall.id.toString() -> this.size = "Chico"
            rBtnMedium.id.toString() -> this.size = "Mediano"
            rBtnBig.id.toString() -> this.size = "Grande"
            else -> {
                validationError += "Debe seleccionar un tamaño"
                resultado = false
            }
        }

        this.weight = v.findViewById<EditText>(R.id.txtInputWeigth).text.toString()
        if (this.weight == "") {
            validationError += "Debe ingresar un peso"
            resultado = false
        }

        when (rgCastrated.checkedRadioButtonId.toString()) {
            rBtnCastratedYes.id.toString() -> this.castrated = "Si"
            rBtnCastratedNo.id.toString() -> this.castrated = "No"
            rBtnCastratedNoIdea.id.toString() -> this.castrated = "NS/NC"
            else -> {
                validationError += "Debe seleccionar si la mascota está castrada"
                resultado = false
            }
        }

        this.description = v.findViewById<EditText>(R.id.txtInputDescription).text.toString()

        when (rgTracing.checkedRadioButtonId.toString()) {
            btnTracingYes.id.toString() -> this.tracing = "true"
            btnTracingNo.id.toString() -> this.tracing = "false"
            else -> {
                validationError += "Debe seleccionar si desea seguimiento"
                resultado = false
            }
        }

        if (address == null && !isForEdit) {
            validationError += "Debe ingresar una dirección"
            resultado = false
        }
        if (isForEdit) {
            latitude = post.latitude
            longitude = post.longitude
        }

        if (!arePhotosLoaded && !isForEdit) {
            validationError += "Debe subir al menos una foto"
            resultado = false
        }

        return resultado
    }

    private fun setupPlacesAutocomplete() {
        val autocompleteFragment = childFragmentManager
            .findFragmentById(R.id.autocomplete_fragment) as AutocompleteSupportFragment
        autocompleteFragment.setPlaceFields(placeFields)

        autocompleteFragment.setHint("Buscar")
        val etTextInput: EditText? =
            autocompleteFragment.view?.findViewById(R.id.places_autocomplete_search_input)
        if (etTextInput != null) {
            etTextInput.setTextColor(Color.WHITE)
        }

        /* if (post.title != "") {
             val geocode = Geocoder(v.context)
             var addressFull = geocode.getFromLocation(
                 post.latitude,
                 post.longitude,
                 1
             )*/

        if (post.title != "") {
            autocompleteFragment.view?.visibility = View.GONE
            textView16.visibility = View.GONE
        }

        autocompleteFragment.setOnPlaceSelectedListener(object : PlaceSelectionListener {
            override fun onPlaceSelected(p0: Place) {
                address = p0
                latitude = p0.latLng?.latitude!!
                longitude = p0.latLng?.longitude!!

            }

            override fun onError(p0: Status) {
            }
        })
        autocompleteFragment.view?.findViewById<View>(R.id.places_autocomplete_clear_button)
            ?.setOnClickListener() {
                address = null
                autocompleteFragment.setText("")
            }
    }

    private fun initPlaces() {
        Places.initialize(v.context, "AIzaSyCFg3S_ycV418XGqN1FctIsT5S02XXqdcE")
        placesClient = Places.createClient(v.context)
    }

    private fun setInfoInElements() {
        var editable = Editable.Factory.getInstance()
        txtTitle.text = editable.newEditable(post.title)
        txtInputName.text = editable.newEditable(post.petName)
        if (post.petType == "Perro") rBtnDog.isChecked = true else rBtnCat.isChecked = true
        if (post.petSex == "Hembra") rBtnFemale.isChecked = true else rBtnMale.isChecked = true
        if (post.postType == "Adopcion") rBtnAdoption.isChecked = true else rBtnLost.isChecked =
            true
        txtInputRace.text = editable.newEditable(post.petBreed)
        if (post.petAge == "Veterano") {
            rBtnVeteran.isChecked = true
        } else if (post.petAge == "Adulto") {
            rBtnAdult.isChecked = true
        } else if (post.petAge == "Joven") {
            rBtnYoung.isChecked = true
        } else {
            rBtnPuppy.isChecked = true
        }
        if (post.petSize == "Chico") {
            rBtnSmall.isChecked = true
        } else if (post.petSize == "Mediano") {
            rBtnMedium.isChecked = true
        } else {
            rBtnBig.isChecked = true
        }

        txtInputWeigth.text = editable.newEditable(post.petWeigth)
        if (post.petCastrated == "Si") {
            rBtnCastratedYes.isChecked = true
        } else if (post.petCastrated == "No") {
            rBtnCastratedNo.isChecked = true
        } else {
            rBtnCastratedNoIdea.isChecked = true
        }

        txtInputDescription.text = editable.newEditable(post.postDescription)
        if (post.petMonitoring) {
            btnTracingYes.isChecked = true
        } else {
            btnTracingNo.isChecked = true
        }
    }

    private fun setElementsVariables() {
        photosLayout = v.findViewById(R.id.photosLayout)
        btnChoose = v.findViewById(R.id.btnChooseImg)
        btnSubmit = v.findViewById(R.id.btnSubmitPost)
        textView11 = v.findViewById(R.id.textView11)
        txtTitle = v.findViewById(R.id.txtTitle)
        txtInputName = v.findViewById(R.id.txtInputName)
        rgAnimal = v.findViewById(R.id.rgAnimal)
        rBtnDog = v.findViewById(R.id.rBtnDog)
        rBtnCat = v.findViewById(R.id.rBtnCat)
        rgSex = v.findViewById(R.id.rgSex)
        rBtnMale = v.findViewById(R.id.rBtnMale)
        rBtnFemale = v.findViewById(R.id.rBtnFemale)
        rgPublicationType = v.findViewById(R.id.rgPublicationType)
        rBtnLost = v.findViewById(R.id.rBtnLost)
        rBtnAdoption = v.findViewById(R.id.rBtnAdoption)
        txtInputRace = v.findViewById(R.id.txtInputRace)
        rgAge = v.findViewById(R.id.rgAge)
        rBtnPuppy = v.findViewById(R.id.rBtnPuppy)
        rBtnYoung = v.findViewById(R.id.rBtnYoung)
        rBtnAdult = v.findViewById(R.id.rBtnAdult)
        rBtnVeteran = v.findViewById(R.id.rBtnVeteran)
        rgSize = v.findViewById(R.id.rgSize)
        rBtnSmall = v.findViewById(R.id.rBtnSmall)
        rBtnMedium = v.findViewById(R.id.rBtnMedium)
        rBtnBig = v.findViewById(R.id.rBtnBig)
        txtInputWeigth = v.findViewById(R.id.txtInputWeigth)
        rgCastrated = v.findViewById(R.id.rgCastrated)
        rBtnCastratedYes = v.findViewById(R.id.rBtnCastratedYes)
        rBtnCastratedNo = v.findViewById(R.id.rBtnCastratedNo)
        rBtnCastratedNoIdea = v.findViewById(R.id.rBtnCastratedNoIdea)
        txtInputDescription = v.findViewById(R.id.txtInputDescription)
        rgTracing = v.findViewById(R.id.rgTracing)
        btnTracingYes = v.findViewById(R.id.btnTracingYes)
        btnTracingNo = v.findViewById(R.id.btnTracingNo)
    }

}